package rdbutils

import (
	"encoding/json"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"xorm.io/builder"
	"xorm.io/xorm"
)

func Query(s *xorm.Session, b *builder.Builder) (ResultSet, error) {
	result, err := s.Query(b)
	if err != nil {
		return nil, errors.Wrap(err, "Query() failed")
	}

	var rs ResultSet
	for i := range result {
		rs = append(rs, result[i])
	}

	return rs, nil
}

type ResultItem map[string][]byte

func (r ResultItem) Int64OrErr(name string) (int64, error) {
	n, err := strconv.ParseInt(string(r[name]), 10, 64)
	if err != nil {
		return 0, errors.Wrap(err, "invalid int64")
	}

	return n, nil
}

func (r ResultItem) Int64(name string) int64 {
	v, _ := r.Int64OrErr(name)
	return v
}

func (r ResultItem) UInt64OrErr(name string) (uint64, error) {
	n, err := strconv.ParseUint(string(r[name]), 10, 64)
	if err != nil {
		return 0, errors.Wrap(err, "invalid uint64")
	}

	return n, nil
}

func (r ResultItem) UInt64(name string) uint64 {
	v, _ := r.UInt64OrErr(name)
	return v
}

func (r ResultItem) IntOrErr(name string) (int, error) {
	n, err := strconv.ParseInt(string(r[name]), 10, 32)
	if err != nil {
		return 0, errors.Wrap(err, "invalid int")
	}

	return int(n), nil
}

func (r ResultItem) Int(name string) int {
	v, _ := r.IntOrErr(name)
	return v
}

func (r ResultItem) UIntOrErr(name string) (uint, error) {
	n, err := strconv.ParseInt(string(r[name]), 10, 32)
	if err != nil {
		return 0, errors.Wrap(err, "invalid uint")
	}

	return uint(n), nil
}

func (r ResultItem) UInt(name string) uint {
	v, _ := r.UIntOrErr(name)
	return v
}

func (r ResultItem) TimeOrErr(name string) (time.Time, error) {
	s := string(r[name])

	t, err := time.Parse(time.RFC3339, s)
	if err != nil {
		return time.Time{}, errors.Wrap(err, "invalid time")
	}

	return t, nil
}

func (r ResultItem) Time(name string) time.Time {
	v, _ := r.TimeOrErr(name)
	return v
}

func (r ResultItem) Bytes(name string) []byte {
	return r[name]
}

func (r ResultItem) String(name string) string {
	return string(r[name])
}

func (r ResultItem) Bool(name string) bool {
	return r.String(name) != "0"
}

func (r ResultItem) Json(name string, v interface{}) error {
	if err := json.Unmarshal(r[name], v); err != nil {
		return errors.Wrap(err, "invalid json")
	}
	return nil
}

type ResultSet []ResultItem

func (s ResultSet) Int64OrErr(i int, name string) (int64, error)    { return s[i].Int64OrErr(name) }
func (s ResultSet) Int64(i int, name string) int64                  { return s[i].Int64(name) }
func (s ResultSet) UInt64OrErr(i int, name string) (uint64, error)  { return s[i].UInt64OrErr(name) }
func (s ResultSet) UInt64(i int, name string) uint64                { return s[i].UInt64(name) }
func (s ResultSet) IntOrErr(i int, name string) (int, error)        { return s[i].IntOrErr(name) }
func (s ResultSet) Int(i int, name string) int                      { return s[i].Int(name) }
func (s ResultSet) UIntOrErr(i int, name string) (uint, error)      { return s[i].UIntOrErr(name) }
func (s ResultSet) UInt(i int, name string) uint                    { return s[i].UInt(name) }
func (s ResultSet) TimeOrErr(i int, name string) (time.Time, error) { return s[i].TimeOrErr(name) }
func (s ResultSet) Time(i int, name string) time.Time               { return s[i].Time(name) }
func (s ResultSet) Bytes(i int, name string) []byte                 { return s[i].Bytes(name) }
func (s ResultSet) String(i int, name string) string                { return s[i].String(name) }
func (s ResultSet) Bool(i int, name string) bool                    { return s[i].Bool(name) }
func (s ResultSet) Json(i int, name string, v interface{}) error    { return s[i].Json(name, v) }
