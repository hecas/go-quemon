package rdbutils

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"sort"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/go-redsync/redsync/v4"
	"github.com/go-redsync/redsync/v4/redis/goredis/v8"
	"github.com/pkg/errors"
	"xorm.io/xorm"
)

type logTableMapper struct {
	ID           int64
	CreationTime time.Time
	Name         string
}

// logTable을 사용하여 sqlsPath의 내용과 비교하여 새로운 SQL문을 실행
// SQL 파일명은 일반적으로 yyyy-MM-dd_0001.sql 로 작성한다.
// MariaDB의 경우 DDL은 자동으로 커밋되기 때문에 사용 시 주의
func Migrate(session *xorm.Session, logTable string, sqlsPath string) error {
	if err := session.Table(logTable).CreateTable(&logTableMapper{}); err != nil {
		return errors.Wrap(err, "CreateTable() failed")
	}

	// 맨 앞이 최근 로그
	var logs []logTableMapper
	if err := session.Table(logTable).OrderBy("id DESC").Find(&logs); err != nil {
		return errors.Wrap(err, "Find() failed")
	}

	lastLogIndex := -1
	if len(logs) > 0 {
		lastLogIndex = 0
	}

	files, err := ioutil.ReadDir(sqlsPath)
	if err != nil {
		return errors.Wrap(err, "ReadDir() failed")
	}

	// 오름차순
	sort.SliceStable(files, func(i, j int) bool {
		return strings.Compare(files[i].Name(), files[j].Name()) <= 0
	})

	var sqlFiles []fs.FileInfo
	for i := range files {
		if files[i].IsDir() {
			continue
		}
		if !strings.HasSuffix(files[i].Name(), ".sql") {
			continue
		}

		// 최근 로그보다 오래된 실행문 제외
		if lastLogIndex >= 0 && strings.Compare(logs[lastLogIndex].Name, files[i].Name()) >= 0 {
			continue
		}

		sqlFiles = append(sqlFiles, files[i])
	}

	if len(sqlFiles) == 0 {
		return nil
	}

	for i := range sqlFiles {
		b, err := ioutil.ReadFile(sqlsPath + "/" + sqlFiles[i].Name())
		if err != nil {
			return errors.Wrap(err, "ReadFile() failed")
		}

		script := ""
		rd := bufio.NewReader(bytes.NewBuffer(b))
		for {
			// 줄 단위로 읽음
			s, err := rd.ReadString('\n')
			s = strings.Trim(s, "\n")

			// 주석 제외
			commentIdx := strings.Index(s, "--")
			if commentIdx == -1 {
				script += s
			} else {
				script += s[:commentIdx]
			}
			script += "\n"

			if err == io.EOF {
				break
			}
		}

		s := strings.Split(script, ";")
		for j := range s {
			sql := strings.TrimSpace(s[j])
			if len(sql) > 0 {
				rs, err := session.Exec(s[j])
				if err != nil {
					return errors.Wrap(err, "Exec() failed")
				}
				_ = rs
			}
		}

		if _, err := session.Table(logTable).Insert(&logTableMapper{
			CreationTime: time.Now(),
			Name:         sqlFiles[i].Name(),
		}); err != nil {
			return errors.Wrap(err, "Insert() failed")
		}
	}

	return nil
}

func MigrateWithLock(db *xorm.Engine, rds *redis.Client, logTable string, sqlsPath string) {
	rdlock := redsync.New(goredis.NewPool(rds))
	rdMutex := rdlock.NewMutex(fmt.Sprintf("migration:%s", logTable))

	if err := rdMutex.Lock(); err != nil {
		log.Fatal(err)
	}
	defer func() {
		if ok, err := rdMutex.Unlock(); !ok || err != nil {
			log.Fatal(err)
		}
	}()

	dbs := db.NewSession()
	defer dbs.Close()

	err := dbs.Begin()
	if err != nil {
		log.Fatal(err)
	}

	err = Migrate(dbs, logTable, sqlsPath)
	if err != nil {
		log.Fatal(err)
	}

	err = dbs.Commit()
	if err != nil {
		log.Fatal(err)
	}
}
