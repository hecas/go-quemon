package rdbutils

import (
	"github.com/pkg/errors"
	"xorm.io/builder"
	"xorm.io/xorm"
)

func Insert(s *xorm.Session, b *builder.Builder) (id int64, affected int64, _ error) {
	result, err := s.Exec(b)
	if err != nil {
		return 0, 0, errors.Wrap(err, "Exec() failed")
	}
	id, err = result.LastInsertId()
	if err != nil {
		return 0, 0, errors.Wrap(err, "LastInsertId() failed")
	}
	affected, err = result.RowsAffected()
	if err != nil {
		return 0, 0, errors.Wrap(err, "RowAffected() failed")
	}
	return id, affected, nil
}

func Update(s *xorm.Session, b *builder.Builder) (affected int64, _ error) {
	result, err := s.Exec(b)
	if err != nil {
		return 0, errors.Wrap(err, "Exec() failed")
	}
	affected, err = result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "RowAffected() failed")
	}
	return affected, nil
}
