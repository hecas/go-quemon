package rdbutils

import (
	"reflect"

	"bitbucket.org/hecas/go-quemon/shortcut"
	"github.com/thoas/go-funk"
	"xorm.io/builder"
)

var Cond cond

type cond struct{}

func (cond) In(col string, sliceOrBuilder interface{}) builder.Cond {
	return Cond.processSliceOrBuilder(col, sliceOrBuilder, func(col string, args ...interface{}) builder.Cond {
		return builder.In(col, args...)
	})
}

func (cond) NotIn(col string, sliceOrBuilder interface{}) builder.Cond {
	return Cond.processSliceOrBuilder(col, sliceOrBuilder, func(col string, args ...interface{}) builder.Cond {
		return builder.NotIn(col, args...)
	})
}

func (cond) processSliceOrBuilder(col string, sliceOrBuilder interface{},
	delegate func(col string, args ...interface{}) builder.Cond) builder.Cond {
	value := reflect.ValueOf(sliceOrBuilder)
	kind := value.Kind()

	if kind == reflect.Slice {
		args := func() []interface{} {
			v := funk.Map(sliceOrBuilder, shortcut.ToInterface)
			v = funk.Uniq(v)
			return v.([]interface{})
		}()
		if len(args) <= 0 {
			return nil
		}

		return delegate(col, args...)
	}

	if b, ok := sliceOrBuilder.(*builder.Builder); ok {
		if b == nil {
			return nil
		}

		return delegate(col, b)
	}

	return nil
}

func (cond) GreaterThan(col string, arg interface{}) builder.Cond {
	return Cond.processSingleArgument(col, arg, func(col string, arg interface{}) builder.Cond {
		return builder.Gt{col: arg}
	})
}

func (cond) GreaterThanOrEqual(col string, arg interface{}) builder.Cond {
	return Cond.processSingleArgument(col, arg, func(col string, arg interface{}) builder.Cond {
		return builder.Gte{col: arg}
	})
}

func (cond) LessThan(col string, arg interface{}) builder.Cond {
	return Cond.processSingleArgument(col, arg, func(col string, arg interface{}) builder.Cond {
		return builder.Lt{col: arg}
	})
}

func (cond) LessThanOrEqual(col string, arg interface{}) builder.Cond {
	return Cond.processSingleArgument(col, arg, func(col string, arg interface{}) builder.Cond {
		return builder.Lte{col: arg}
	})
}

func (cond) processSingleArgument(col string, arg interface{},
	delegate func(col string, arg interface{}) builder.Cond) builder.Cond {
	value := reflect.ValueOf(arg)
	kind := value.Kind()

	if kind == reflect.Ptr && value.IsNil() {
		return nil
	}

	return delegate(col, arg)
}
