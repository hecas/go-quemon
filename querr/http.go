package querr

import (
	"net/http"

	"bitbucket.org/hecas/go-quemon/msgerr"
)

var httpCodes = map[error]int{
	errOK:           http.StatusOK,
	errBadRequest:   http.StatusBadRequest,
	errDenied:       http.StatusForbidden,
	errNotFound:     http.StatusNotFound,
	errConflict:     http.StatusConflict,
	errServerError:  http.StatusInternalServerError,
	errServiceFault: http.StatusServiceUnavailable,
}

func HTTPCode(err error) int {
	converse := func(err error) int {
		code, ok := httpCodes[err]
		if ok {
			return code
		}
		return 0
	}
	code := converse(err)
	if code > 0 {
		return code
	}
	msge, ok := err.(*msgerr.Error)
	if ok {
		code := converse(msge.Raw())
		if code > 0 {
			return code
		}
	}
	return http.StatusInternalServerError
}

func FromHTTP(code int) error {
	for err := range httpCodes {
		if httpCodes[err] == code {
			return err
		}
	}
	return errServerError
}
