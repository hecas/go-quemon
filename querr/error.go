package querr

import (
	"errors"

	"bitbucket.org/hecas/go-quemon/msgerr"
)

var (
	errOK           = errors.New("ErrOK")
	errBadRequest   = errors.New("ErrBadRequest")
	errDenied       = errors.New("ErrDenied")
	errNotFound     = errors.New("ErrNotFound")
	errConflict     = errors.New("ErrConflict")
	errServerError  = errors.New("ErrServerError")
	errServiceFault = errors.New("ErrServiceFault")
	errs            = map[error]struct{}{
		errOK:           {},
		errBadRequest:   {},
		errDenied:       {},
		errNotFound:     {},
		errConflict:     {},
		errServerError:  {},
		errServiceFault: {},
	}
)

func ErrOK() error           { return errOK }
func ErrBadRequest() error   { return errBadRequest }
func ErrDenied() error       { return errDenied }
func ErrNotFound() error     { return errNotFound }
func ErrConflict() error     { return errConflict }
func ErrServerError() error  { return errServerError }
func ErrServiceFault() error { return errServiceFault }

func IsOK(err error) bool           { return compareError(err, errOK) }
func IsBadRequest(err error) bool   { return compareError(err, errBadRequest) }
func IsDenied(err error) bool       { return compareError(err, errDenied) }
func IsNotFound(err error) bool     { return compareError(err, errNotFound) }
func IsConflict(err error) bool     { return compareError(err, errConflict) }
func IsServerError(err error) bool  { return compareError(err, errServerError) }
func IsServiceFault(err error) bool { return compareError(err, errServiceFault) }

func compareError(in error, err error) bool {
	if msgerr.CompareRaw(in, err) {
		return true
	}
	return in == err
}
