package querr

import "bitbucket.org/hecas/go-quemon/msgerr"

func MsgErrOK(format string, keyValues ...string) *msgerr.Error {
	return msgerr.NewWith(errOK, format, keyValues...)
}

func MsgErrBadRequest(format string, keyValues ...string) *msgerr.Error {
	return msgerr.NewWith(errBadRequest, format, keyValues...)
}

func MsgErrDenied(format string, keyValues ...string) *msgerr.Error {
	return msgerr.NewWith(errDenied, format, keyValues...)
}

func MsgErrNotFound(format string, keyValues ...string) *msgerr.Error {
	return msgerr.NewWith(errNotFound, format, keyValues...)
}

func MsgErrConflict(format string, keyValues ...string) *msgerr.Error {
	return msgerr.NewWith(errConflict, format, keyValues...)
}

func MsgErrServerError(format string, keyValues ...string) *msgerr.Error {
	return msgerr.NewWith(errServerError, format, keyValues...)
}

func MsgErrServiceFault(format string, keyValues ...string) *msgerr.Error {
	return msgerr.NewWith(errServiceFault, format, keyValues...)
}

func MsgDescriptorOK(format string, keyValues ...string) msgerr.Descriptor {
	return msgerr.MakeDescriptor(MsgErrOK(format, keyValues...))
}

func MsgDescriptorBadRequest(format string, keyValues ...string) msgerr.Descriptor {
	return msgerr.MakeDescriptor(MsgErrBadRequest(format, keyValues...))
}

func MsgDescriptorDenied(format string, keyValues ...string) msgerr.Descriptor {
	return msgerr.MakeDescriptor(MsgErrDenied(format, keyValues...))
}

func MsgDescriptorNotFound(format string, keyValues ...string) msgerr.Descriptor {
	return msgerr.MakeDescriptor(MsgErrNotFound(format, keyValues...))
}

func MsgDescriptorConflict(format string, keyValues ...string) msgerr.Descriptor {
	return msgerr.MakeDescriptor(MsgErrConflict(format, keyValues...))
}

func MsgDescriptorServerError(format string, keyValues ...string) msgerr.Descriptor {
	return msgerr.MakeDescriptor(MsgErrServerError(format, keyValues...))
}

func MsgDescriptorServiceFault(format string, keyValues ...string) msgerr.Descriptor {
	return msgerr.MakeDescriptor(MsgErrServiceFault(format, keyValues...))
}

func ResultMessage(desc msgerr.Descriptor, result interface{}) map[string]interface{} {
	if result == nil {
		result = struct{}{}
	}
	return map[string]interface{}{
		"error":   desc.Error,
		"message": desc.Message,
		"result":  result,
	}
}

func MsgDescriptor(err error) msgerr.Descriptor {
	msge, ok := err.(*msgerr.Error)
	if ok {
		return msgerr.MakeDescriptor(msge)
	}
	_, ok = errs[err]
	if !ok {
		return MsgDescriptorServerError(err.Error())
	}
	return msgerr.MakeDescriptor(err)
}
