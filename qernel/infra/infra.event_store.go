package infra

import (
	"encoding/json"
	"time"

	"bitbucket.org/hecas/go-quemon/qernel/model"
	"github.com/pkg/errors"
	"xorm.io/xorm"
)

type eventStoreItem struct {
	ID          string    `xorm:"varchar(48) 'id' notnull unique(version)"`
	Version     int       `xorm:"int(11) 'version' notnull unique(version)"`
	FiredTime   time.Time `xorm:"datetime(3) 'fired_time' notnull"`
	Class       string    `xorm:"varchar(30) 'class' notnull"`
	Status      string    `xorm:"varchar(20) 'status' notnull"`
	Payload     string    `xorm:"longtext 'payload' notnull"`
	PublishTime time.Time `xorm:"datetime(3) 'publish_time'"`
	PublishTry  int       `xorm:"int(11) 'publish_try'"`
}

type EventStore struct {
	xormSess *xorm.Session
	events   map[string][]*model.DomainEvent
}

func NewEventStore(xormSess *xorm.Session) *EventStore {
	return &EventStore{
		xormSess: xormSess,
		events:   make(map[string][]*model.DomainEvent),
	}
}

func (s *EventStore) Append(store string, events ...*model.DomainEvent) error {
	items := []eventStoreItem{}
	for _, event := range events {
		items = append(items, eventStoreItem{
			ID:        event.ID,
			Version:   event.Version,
			FiredTime: event.FiredTime,
			Status:    "trying",
			Class:     event.Class,
			Payload: func() string {
				b, _ := json.Marshal(event.Payload)
				return string(b)
			}(),
		})
	}
	_, err := s.xormSess.Table(store).InsertMulti(&items)
	if err != nil {
		return errors.WithStack(err)
	}
	s.events[store] = append(s.events[store], events...)
	return nil
}

func (s *EventStore) AppendedStore() []string {
	stores := make([]string, 0, len(s.events))
	for store := range s.events {
		stores = append(stores, store)
	}
	return stores
}
