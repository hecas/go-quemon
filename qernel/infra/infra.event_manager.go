package infra

import (
	"context"
	"sync"

	"bitbucket.org/hecas/go-quemon/querr"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"xorm.io/xorm"
)

type EventManager struct {
	amqpConn   *amqp.Connection
	xormEngn   *xorm.Engine
	exchange   string
	publishers map[string]*eventStorePublisher
	mu         sync.RWMutex
}

func NewEventManager(
	amqpConn *amqp.Connection,
	xormEngn *xorm.Engine,
	exchange string,
) *EventManager {
	m := EventManager{
		amqpConn:   amqpConn,
		xormEngn:   xormEngn,
		exchange:   exchange,
		publishers: make(map[string]*eventStorePublisher),
	}
	return &m
}

func (m *EventManager) PublishStore(ctx context.Context, store string) error {
	amqpChan, err := m.amqpConn.Channel()
	if err != nil {
		return errors.WithStack(err)
	}
	publisher, err := newEventStorePublisher(amqpChan, m.xormEngn, m.exchange, store)
	if err != nil {
		amqpChan.Close()
		return errors.WithStack(err)
	}
	err = func() error {
		m.mu.Lock()
		defer m.mu.Unlock()
		_, ok := m.publishers[store]
		if ok {
			return querr.MsgErrConflict("이미 저장소가 존재합니다.")
		}
		m.publishers[store] = publisher
		return nil
	}()
	if err != nil {
		amqpChan.Close()
		return err
	}
	go publisher.run(ctx)
	return nil
}

func (m *EventManager) Subscribe(ctx context.Context, queue string, key string, onRead func(string, []byte)) error {
	amqpChan, err := m.amqpConn.Channel()
	if err != nil {
		return errors.WithStack(err)
	}
	subscriber, err := newEventSubscriber(amqpChan, m.exchange, queue, key, onRead)
	if err != nil {
		amqpChan.Close()
		return errors.WithStack(err)
	}
	go subscriber.run(ctx)
	return nil
}

func (m *EventManager) RequestPublish(stores ...string) {
	m.mu.RLock()
	defer m.mu.RUnlock()
	for i := range stores {
		store := stores[i]
		publisher, ok := m.publishers[store]
		if ok {
			publisher.requestPublish()
		}
	}
}
