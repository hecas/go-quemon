package infra

import (
	"bitbucket.org/hecas/go-quemon/qernel/model"
	"bitbucket.org/hecas/go-quemon/querr"
	"github.com/pkg/errors"
	"xorm.io/builder"
	"xorm.io/xorm"
)

type Entity interface {
	GetID() string
	GetRootVersion() int
	GetHistory() []*model.DomainEvent
	RebaseHistory()
}

type Repository[T Entity] struct {
	xormSess       *xorm.Session
	eventPublisher *EventStore
	eventStore     string
	newEntity      func() T
}

func NewRepository[T Entity](
	xormSess *xorm.Session,
	eventPublisher *EventStore,
	eventStore string,
	newEntity func() T,
) *Repository[T] {
	return &Repository[T]{
		xormSess:       xormSess,
		eventPublisher: eventPublisher,
		eventStore:     eventStore,
		newEntity:      newEntity,
	}
}

func (r *Repository[T]) GetEntity(query func(xormSess *xorm.Session)) (T, error) {
	query(r.xormSess)
	entity := r.newEntity()
	ok, err := r.xormSess.Get(entity)
	if err != nil {
		return entity, errors.WithStack(err)
	}
	if !ok {
		return entity, querr.MsgErrNotFound("찾을 수 없습니다.")
	}
	entity.RebaseHistory()
	return entity, nil
}

func (r *Repository[T]) FindEntity(query func(xormSess *xorm.Session)) ([]T, error) {
	query(r.xormSess)
	var entities []T
	err := r.xormSess.Find(&entities)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	for i := range entities {
		entities[i].RebaseHistory()
	}
	return entities, nil
}

func (r *Repository[T]) Save(entity T) error {
	events := entity.GetHistory()
	rootVersion := entity.GetRootVersion()
	if len(events) <= 0 {
		return nil
	}
	if rootVersion == 0 {
		_, err := r.xormSess.Insert(entity)
		if err != nil {
			return errors.WithStack(err)
		}
	} else {
		affected, err := r.xormSess.Where(builder.Eq{
			"id":      entity.GetID(),
			"version": rootVersion,
		}).Update(entity)
		if err != nil {
			return errors.WithStack(err)
		}
		if affected <= 0 {
			return querr.MsgErrConflict("다시 시도하여 주십시오.")
		}
	}
	entity.RebaseHistory()
	if r.eventPublisher != nil {
		return r.eventPublisher.Append(r.eventStore, events...)
	}
	return nil
}
