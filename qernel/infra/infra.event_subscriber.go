package infra

import (
	"context"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

type eventSubscriber struct {
	amqpChan *amqp.Channel
	readChan <-chan amqp.Delivery
	onRead   func(key string, data []byte)
}

func newEventSubscriber(
	amqpChan *amqp.Channel,
	exchange string,
	queue string,
	key string,
	onRead func(key string, data []byte),
) (*eventSubscriber, error) {
	durable := queue != ""
	autoDelete := queue == ""
	exclusive := queue == ""
	var readChan <-chan amqp.Delivery
	err := func() error {
		err := amqpChan.ExchangeDeclare(exchange, "topic", true, false, false, false, nil)
		if err != nil {
			return errors.WithStack(err)
		}
		que, err := amqpChan.QueueDeclare(queue, durable, autoDelete, exclusive, false, nil)
		if err != nil {
			return errors.WithStack(err)
		}
		err = amqpChan.QueueBind(que.Name, key, exchange, false, nil)
		if err != nil {
			return errors.WithStack(err)
		}
		readChan, err = amqpChan.Consume(que.Name, "", true, exclusive, false, false, nil)
		if err != nil {
			return errors.WithStack(err)
		}
		return nil
	}()
	if err != nil {
		return nil, err
	}
	return &eventSubscriber{
		amqpChan: amqpChan,
		readChan: readChan,
		onRead:   onRead,
	}, nil
}

func (s *eventSubscriber) run(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			s.amqpChan.Close()
			return
		case msg := <-s.readChan:
			s.onRead(msg.RoutingKey, msg.Body)
		}
	}
}
