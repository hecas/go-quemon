package infra

import (
	"sync"
	"time"
)

type Object interface {
	ID() string
}

type LocalObjectManager[T Object] struct {
	mu             sync.RWMutex
	managedObjects map[string]managedLocalObject[T]
}

func NewLocalObjectManager[T Object]() *LocalObjectManager[T] {
	return &LocalObjectManager[T]{
		managedObjects: make(map[string]managedLocalObject[T]),
	}
}

type managedLocalObject[T Object] struct {
	refCount        int
	lastReleaseTime time.Time
	obj             T
}

func (o *managedLocalObject[T]) isDead(expiration time.Time) bool {
	return o.refCount <= 0 && o.lastReleaseTime.Before(expiration)
}

func (m *LocalObjectManager[T]) AcquireObject(id string, getIfNotFound func(id string) T) (ret T) {
	m.mu.Lock()
	defer m.mu.Unlock()
	managedObj, ok := m.managedObjects[id]
	if !ok {
		if getIfNotFound == nil {
			return ret
		}
		managedObj = managedLocalObject[T]{
			obj: getIfNotFound(id),
		}
	}
	managedObj.refCount++
	m.managedObjects[id] = managedObj
	return managedObj.obj
}

func (m *LocalObjectManager[T]) ReleaseObject(id string, purge bool) {
	m.mu.Lock()
	defer m.mu.Unlock()
	managedObj, ok := m.managedObjects[id]
	if !ok {
		return
	}
	managedObj.refCount--
	managedObj.lastReleaseTime = time.Now()
	if purge && managedObj.refCount <= 0 {
		delete(m.managedObjects, id)
	} else {
		m.managedObjects[id] = managedObj
	}
}

func (m *LocalObjectManager[T]) Visit(job func(objects map[string]T)) {
	objects := func() map[string]T {
		m.mu.RLock()
		defer m.mu.RUnlock()
		objects := make(map[string]T)
		for id, managedObj := range m.managedObjects {
			objects[id] = managedObj.obj
		}
		return objects
	}()
	job(objects)
}

func (m *LocalObjectManager[T]) PurgeObject(expiration time.Time) {
	deleting := func() []string {
		m.mu.RLock()
		defer m.mu.RUnlock()
		var deleting []string
		for id, managedObj := range m.managedObjects {
			if managedObj.isDead(expiration) {
				deleting = append(deleting, id)
			}
		}
		return deleting
	}()
	m.mu.Lock()
	defer m.mu.Unlock()
	for _, id := range deleting {
		managedObj, ok := m.managedObjects[id]
		if ok && managedObj.isDead(expiration) {
			delete(m.managedObjects, id)
		}
	}
}
