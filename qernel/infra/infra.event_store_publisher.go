package infra

import (
	"context"
	"encoding/json"
	"time"

	"github.com/labstack/gommon/log"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"xorm.io/builder"
	"xorm.io/xorm"
)

type eventStorePublisher struct {
	amqpChan *amqp.Channel
	xormEngn *xorm.Engine
	exchange string
	store    string
	reqChan  chan struct{}
}

func newEventStorePublisher(
	amqpChan *amqp.Channel,
	xormEngn *xorm.Engine,
	exchange string,
	store string,
) (*eventStorePublisher, error) {
	err := amqpChan.ExchangeDeclare(exchange, "topic", true, false, false, false, nil)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	err = xormEngn.Table(store).Sync2(&eventStoreItem{})
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return &eventStorePublisher{
		amqpChan: amqpChan,
		xormEngn: xormEngn,
		exchange: exchange,
		store:    store,
		reqChan:  make(chan struct{}, 10),
	}, nil
}

func (p *eventStorePublisher) run(ctx context.Context) {
	timer := time.NewTimer(1 * time.Millisecond)
	defer timer.Stop()
	for {
		select {
		case <-ctx.Done():
			p.amqpChan.Close()
			return
		case <-p.reqChan:
			len := len(p.reqChan)
			if len > 0 {
				for i := 0; i < len; i++ {
					<-p.reqChan
				}
			}
			p.tryPublish()
		case <-timer.C:
			p.tryPublish()
			timer.Reset(1 * time.Second)
		}
	}
}

func (p *eventStorePublisher) tryPublish() {
	xormSess := p.xormEngn.NewSession()
	defer xormSess.Close()
	xormSess.Begin()
	var items []eventStoreItem
	err := xormSess.Table(p.store).
		NotIn("status", "published", "failed").
		OrderBy("fired_time DESC").
		Limit(100).
		ForUpdate().
		Find(&items)
	if err != nil {
		log.Errorf("%+v", err)
		return
	}
	p.publish(xormSess, items)
	xormSess.Commit()
}

func (p *eventStorePublisher) publish(xormSess *xorm.Session, items []eventStoreItem) {
	for _, item := range items {
		item.PublishTime = time.Now()
		item.PublishTry += 1
		err := p.amqpChan.Publish(p.exchange, item.Class, true, false, amqp.Publishing{
			ContentType: "application/json",
			Body: func() []byte {
				b, _ := json.Marshal(map[string]any{
					"id":         item.ID,
					"version":    item.Version,
					"fired_time": item.FiredTime,
					"class":      item.Class,
					"payload":    json.RawMessage(item.Payload),
				})
				return b
			}(),
		})
		if err != nil {
			if item.PublishTry >= 10 {
				item.Status = "failed"
			}
		} else {
			item.Status = "published"
		}
		_, err = xormSess.Table(p.store).
			Where(builder.Eq{
				"id":      item.ID,
				"version": item.Version,
			}).
			Update(&item)
		if err != nil {
			log.Errorf("%+v", err)
		}
	}
}

func (p *eventStorePublisher) requestPublish() {
	p.reqChan <- struct{}{}
}
