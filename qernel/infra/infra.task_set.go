package infra

import "context"

type TaskSet struct {
	handles map[string]TaskHandlerFunc
}

type TaskHandlerFunc func(ctx context.Context) error

func (s *TaskSet) Add(name string, handle TaskHandlerFunc) {
	if s.handles == nil {
		s.handles = make(map[string]TaskHandlerFunc)
	}
	s.handles[name] = handle
}

func (s *TaskSet) Go(ctx context.Context, onError func(name string)) {
	for name := range s.handles {
		name := name
		handle := s.handles[name]
		go func() {
			err := handle(ctx)
			if err != nil {
				onError(name)
			}
		}()
	}
}
