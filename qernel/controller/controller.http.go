package controller

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

type HTTPHandler struct {
	Methods []string
	Path    string
	Handle  echo.HandlerFunc
}

func SetupHTTPHandler(ec *echo.Echo, handlers []HTTPHandler, middlewares ...echo.MiddlewareFunc) {
	for _, handler := range handlers {
		for _, method := range handler.Methods {
			log.Infof("HTTP %s %s", method, handler.Path)
			ec.Add(method, handler.Path, handler.Handle, middlewares...)
		}
	}
}
