package controller

import (
	"context"

	"bitbucket.org/hecas/go-quemon/qernel/infra"
	"github.com/labstack/gommon/log"
)

type TaskHandler struct {
	Name   string
	Handle infra.TaskHandlerFunc
}

type TaskHandlerFunc func(ctx context.Context) error

func SetupTaskHandler(taskSet *infra.TaskSet, handlers []TaskHandler) {
	setup := func(handler *TaskHandler) {
		taskSet.Add(handler.Name, handler.Handle)
	}
	for i := range handlers {
		log.Infof("TASK %s", handlers[i].Name)
		setup(&handlers[i])
	}
}
