package controller

import (
	"io"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"github.com/pkg/errors"
)

type WSHandler struct {
	Path        string
	OnConnected func(conn *WSConnection)
	OnClosed    func(conn *WSConnection)
	OnRead      func(conn *WSConnection, reader io.Reader, binary bool)
}

func (h *WSHandler) handleConnected(conn *WSConnection) {
	if h.OnConnected != nil {
		h.OnConnected(conn)
	}
}

func (h *WSHandler) handleClosed(conn *WSConnection) {
	if h.OnClosed != nil {
		h.OnClosed(conn)
	}
}

func (h *WSHandler) handleRead(conn *WSConnection, reader io.Reader, binary bool) {
	if h.OnRead != nil {
		h.OnRead(conn, reader, binary)
	}
}

func SetupWSHandler(ec *echo.Echo, handlers []WSHandler) {
	upgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	writeChBufferSize := 32
	writeWait := 10 * time.Second
	pongWait := 30 * time.Second
	pingPeriod := pongWait * 4 / 5

	setup := func(handler *WSHandler) {
		ec.Add(echo.GET, handler.Path, func(ctx echo.Context) error {
			conn, err := upgrader.Upgrade(ctx.Response(), ctx.Request(), nil)
			if err != nil {
				return err
			}
			defer conn.Close()
			doneCh := make(chan struct{})
			writeCh := make(chan []byte, writeChBufferSize)
			stopCh := make(chan struct{}, 1)
			jobCh := make(chan func())
			wsConn := WSConnection{
				echoCtx: ctx,
				id:      uuid.New().String(),
				storage: make(map[string]any),
				doneCh:  doneCh,
				stopCh:  stopCh,
				writeCh: writeCh,
				jobCh:   jobCh,
			}
			writing := func() {
				ticker := time.NewTicker(pingPeriod)
				defer func() {
					ticker.Stop()
					conn.WriteMessage(websocket.CloseMessage, []byte{})
				}()
				for {
					select {
					case <-wsConn.doneCh:
						return
					case bytes := <-writeCh:
						write := func(data []byte) error {
							conn.SetWriteDeadline(time.Now().Add(writeWait))
							w, err := conn.NextWriter(websocket.TextMessage)
							if err != nil {
								return errors.WithStack(err)
							}
							defer w.Close()
							_, err = w.Write(bytes)
							return err
						}
						err := func() error {
							err := write(bytes)
							if err != nil {
								return err
							}
							bufferedNum := len(wsConn.writeCh)
							for i := 0; i < bufferedNum; i++ {
								bytes = <-writeCh
								err := write(bytes)
								if err != nil {
									return err
								}
							}
							return nil
						}()
						if err != nil {
							wsConn.Disconnect()
							return
						}
					case <-ticker.C:
						conn.SetWriteDeadline(time.Now().Add(writeWait))
						conn.WriteMessage(websocket.PingMessage, nil)
					}
				}
			}
			reading := func() {
				conn.SetReadDeadline(time.Now().Add(pongWait))
				conn.SetPongHandler(func(string) error {
					conn.SetReadDeadline(time.Now().Add(pongWait))
					return nil
				})
				for {
					typ, reader, err := conn.NextReader()
					if err != nil {
						wsConn.Disconnect()
						return
					}
					if typ != websocket.TextMessage && typ != websocket.BinaryMessage {
						continue
					}
					wsConn.DispatchJob(func() {
						handler.handleRead(&wsConn, reader, typ == websocket.BinaryMessage)
					})
				}
			}
			// writing receiver
			go writing()
			// reading receiver
			go reading()
			// closing receiver
			go func() {
				<-stopCh
				close(doneCh)
			}()
			handler.handleConnected(&wsConn)
			defer handler.handleClosed(&wsConn)
			for {
				select {
				case <-wsConn.doneCh:
					return nil
				case job := <-jobCh:
					if job != nil {
						job()
					}
				}
			}
		})
	}
	for i := range handlers {
		log.Infof("WS %s", handlers[i].Path)
		setup(&handlers[i])
	}
}

type WSConnection struct {
	echoCtx echo.Context
	id      string
	storage map[string]any
	doneCh  <-chan struct{}
	stopCh  chan<- struct{}
	writeCh chan<- []byte
	jobCh   chan<- func()
}

func (s *WSConnection) ID() string {
	return s.id
}

func (s *WSConnection) Write(data []byte) {
	select {
	case <-s.doneCh:
	case s.writeCh <- data:
	}
}

func (s *WSConnection) DispatchJob(job func()) {
	select {
	case <-s.doneCh:
	case s.jobCh <- job:
	}
}

func (s *WSConnection) Disconnect() {
	select {
	case s.stopCh <- struct{}{}:
	default:
	}
}

func (s *WSConnection) Set(key string, object any) {
	s.storage[key] = object
}

func (s *WSConnection) Get(key string) any {
	return s.storage[key]
}
