package controller

import (
	"bufio"
	"encoding/json"
	"io"
	"io/ioutil"

	"bitbucket.org/hecas/go-quemon/msgerr"
	"bitbucket.org/hecas/go-quemon/querr"
	"github.com/labstack/gommon/log"
	"github.com/pkg/errors"
)

type SimpleWSRequest struct {
	conn       *WSConnection
	class      string
	id         string
	payload    json.RawMessage
	binaryData []byte
	response   *SimpleWSResponse
}

type SimpleWSResponse struct {
	Status     int
	Descriptor msgerr.Descriptor
	Result     any
}

func NewSimpleWSResponse(status int, desc msgerr.Descriptor, result any) *SimpleWSResponse {
	return &SimpleWSResponse{
		Status:     status,
		Descriptor: desc,
		Result:     result,
	}
}

func (r *SimpleWSRequest) Bind(v any) error {
	if r.payload == nil {
		return nil
	}
	err := json.Unmarshal(r.payload, v)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (r *SimpleWSRequest) Connection() *WSConnection { return r.conn }
func (r *SimpleWSRequest) IsBinary() bool            { return r.payload == nil }
func (r *SimpleWSRequest) BinaryData() []byte        { return r.binaryData }

func (r *SimpleWSRequest) Response(res *SimpleWSResponse) error {
	r.response = res
	return nil
}

type SimpleWSRouter map[string]func(*SimpleWSRequest) error

func (r SimpleWSRouter) Route(request *SimpleWSRequest) error {
	handle, ok := r[request.class]
	if !ok {
		return querr.MsgErrNotFound("알 수 없는 요청입니다.")
	}
	return handle(request)
}

func SimpleWSReadHandler(router SimpleWSRouter) func(conn *WSConnection, reader io.Reader, binary bool) {
	return func(conn *WSConnection, reader io.Reader, binary bool) {
		makeRequest := func(reader io.Reader, binary bool) (*SimpleWSRequest, error) {
			var packet struct {
				Class      string          `json:"class"`
				ID         string          `json:"id"`
				Payload    json.RawMessage `json:"payload"`
				BinaryData []byte          `json:"-"`
			}
			if binary {
				buf := bufio.NewReader(reader)
				b, err := buf.ReadBytes(0)
				if err != nil {
					return nil, errors.WithStack(err)
				}
				err = json.Unmarshal(b[:len(b)-1], &packet)
				if err != nil {
					return nil, errors.WithStack(err)
				}
				binaryData, err := ioutil.ReadAll(buf)
				if err != nil {
					return nil, errors.WithStack(err)
				}
				packet.BinaryData = binaryData
			} else {
				err := json.NewDecoder(reader).Decode(&packet)
				if err != nil {
					return nil, errors.WithStack(err)
				}
			}
			return &SimpleWSRequest{
				conn:       conn,
				id:         packet.ID,
				class:      packet.Class,
				payload:    packet.Payload,
				binaryData: packet.BinaryData,
			}, nil
		}
		makeResponse := func(request *SimpleWSRequest, err error) ([]byte, error) {
			packet := map[string]any{
				"class": request.class,
				"id":    request.id,
			}
			var (
				status  int
				payload struct {
					msgerr.Descriptor
					Result any `json:"result"`
				}
			)
			if err == nil {
				if request.response != nil {
					status = request.response.Status
					payload.Descriptor = request.response.Descriptor
					payload.Result = request.response.Result

				}
			} else {
				status = querr.HTTPCode(err)
				payload.Descriptor = querr.MsgDescriptor(err)
				payload.Result = struct{}{}
			}
			packet["status"] = status
			packet["payload"] = payload
			resBytes, err := json.Marshal(packet)
			if err != nil {
				return nil, errors.WithStack(err)
			}
			return resBytes, nil
		}
		request, err := makeRequest(reader, binary)
		if err != nil {
			log.Errorf("%+v", err)
			return
		}
		response, err := makeResponse(request, router.Route(request))
		if err != nil {
			log.Errorf("%+v", err)
			return
		}
		conn.Write(response)
	}
}

func SimpleWSNotify(conn *WSConnection, class string, response *SimpleWSResponse) error {
	var payload struct {
		msgerr.Descriptor
		Result any `json:"result"`
	}
	payload.Descriptor = response.Descriptor
	payload.Result = response.Result
	packet := map[string]any{
		"class":   class,
		"status":  response.Status,
		"payload": payload,
	}
	b, err := json.Marshal(packet)
	if err != nil {
		return errors.WithStack(err)
	}
	conn.Write(b)
	return nil
}
