package controller

import (
	"context"

	"bitbucket.org/hecas/go-quemon/qernel/infra"
)

func PublishEventTask(
	eventManager *infra.EventManager,
	stores []string,
) infra.TaskHandlerFunc {
	return func(ctx context.Context) error {
		for i := range stores {
			store := stores[i]
			err := eventManager.PublishStore(ctx, store)
			if err != nil {
				return err
			}
		}
		<-ctx.Done()
		return nil
	}
}

type EventSubscription struct {
	Queue  string
	Key    string
	Router EventRouter
}

type EventRouter map[string]func(key string, data []byte) error

func SubscribeEventTask(
	eventManager *infra.EventManager,
	subscriptions []EventSubscription,
) infra.TaskHandlerFunc {
	return func(ctx context.Context) error {
		for i := range subscriptions {
			subscription := subscriptions[i]
			err := eventManager.Subscribe(
				ctx,
				subscription.Queue,
				subscription.Key,
				func(key string, data []byte) {
					handle, ok := subscription.Router[key]
					if ok {
						handle(key, data)
					}
				},
			)
			if err != nil {
				return err
			}
		}
		<-ctx.Done()
		return nil
	}
}
