package model

import "time"

type DomainEvent struct {
	ID        string       `json:"id"`
	Version   int          `json:"version"`
	FiredTime time.Time    `json:"fired_time"`
	Class     string       `json:"class"`
	Payload   EventPayload `json:"payload"`
}

type EventPayload interface {
	Class() string
}

type EventApplier[T any] interface {
	Apply(target *T)
}
