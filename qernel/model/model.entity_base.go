package model

import "time"

type EntityBase struct {
	id          string
	version     *int
	handleEvent func(payload EventPayload)
	rootVersion int
	history     []*DomainEvent
}

func NewEntityBase[T any](
	target *T,
	id string,
	version *int,
) *EntityBase {
	return &EntityBase{
		id:      id,
		version: version,
		handleEvent: func(payload EventPayload) {
			applier, ok := payload.(EventApplier[T])
			if ok {
				applier.Apply(target)
			}
		},
		rootVersion: *version,
		history:     make([]*DomainEvent, 0),
	}
}

func (e *EntityBase) GetID() string              { return e.id }
func (e *EntityBase) GetRootVersion() int        { return e.rootVersion }
func (e *EntityBase) GetHistory() []*DomainEvent { return e.history }

func (e *EntityBase) RaiseEvent(payload EventPayload) {
	e.handleEvent(payload)
	*e.version = e.rootVersion + len(e.history) + 1
	e.history = append(e.history, &DomainEvent{
		ID:        e.id,
		Version:   *e.version,
		FiredTime: time.Now(),
		Class:     payload.Class(),
		Payload:   payload,
	})
}
