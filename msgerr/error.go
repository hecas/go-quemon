package msgerr

import (
	"encoding/json"
	"fmt"
)

type Error struct {
	raw error
	msg Message
}

var _ error = &Error{}

func New(raw error) *Error {
	e, ok := raw.(*Error)
	if ok {
		return e
	}
	return NewWith(raw, raw.Error())
}

func NewWith(raw error, format string, kv ...string) *Error {
	return &Error{
		raw: raw,
		msg: MakeMessage(format, kv...),
	}
}

func (e *Error) Raw() error {
	return e.raw
}

func (e *Error) Error() string {
	return fmt.Sprintf("%s: %s", e.raw, e.msg.String())
}

func (e *Error) MarshalJSON() ([]byte, error) {
	desc := MakeDescriptor(e)
	return json.Marshal(&desc)
}

func CompareRaw(err error, raw error) bool {
	if e, ok := err.(*Error); ok {
		return e.raw == raw
	}
	return false
}
