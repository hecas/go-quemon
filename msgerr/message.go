package msgerr

import "strings"

func MakeMessage(format string, kv ...string) Message {
	return Message{
		Format: format,
		Params: func() map[string]string {
			ret := map[string]string{}
			for i := range kv {
				if i+1 >= len(kv) {
					break
				}
				ret[kv[i]] = kv[i+1]
			}
			return ret
		}(),
	}
}

type Message struct {
	// 메시지 포맷. 예시: `"성공하였습니다: {detail}"`
	Format string `json:"format"`
	// 메시지 인자. 예시: `{ "detail": "성공" }`
	Params map[string]string `json:"params"`
}

func (m *Message) String() string {
	args := make([]string, 0, len(m.Params)*2)
	for k, v := range m.Params {
		args = append(args, "{"+k+"}", v)
	}
	return strings.NewReplacer(args...).Replace(m.Format)
}
