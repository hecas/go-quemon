package msgerr

type Descriptor struct {
	Error   string  `json:"error"`
	Message Message `json:"message"`
}

func MakeDescriptor(err error) Descriptor {
	e := New(err)
	return Descriptor{
		Error: func() string {
			if e.raw == nil {
				return ""
			}
			return e.raw.Error()
		}(),
		Message: e.msg,
	}
}
