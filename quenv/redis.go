package quenv

import "fmt"

type Redis struct {
	hostName     string
	servicePort  uint16
	authPassword string
	index        int
}

func (r *Redis) HostName() string     { return r.hostName }
func (r *Redis) ServicePort() uint16  { return r.servicePort }
func (r *Redis) AuthPassword() string { return r.authPassword }
func (r *Redis) Index() int           { return r.index }

func (r *Redis) Address() string {
	return fmt.Sprintf("%s:%d",
		r.hostName,
		r.servicePort,
	)
}

type RedisMapper struct {
	HostName     string `yaml:"host_name"`
	ServicePort  uint16 `yaml:"service_port"`
	AuthPassword string `yaml:"auth_password"`
	Index        int    `yaml:"index"`
}

func (m *RedisMapper) Build() Redis {
	return Redis{
		hostName:     m.HostName,
		servicePort:  m.ServicePort,
		authPassword: m.AuthPassword,
		index:        m.Index,
	}
}
