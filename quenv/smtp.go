package quenv

type SMTP struct {
	hostName      string
	servicePort   uint16
	authName      string
	authPassword  string
	senderAddress string
}

func (s *SMTP) HostName() string      { return s.hostName }
func (s *SMTP) ServicePort() uint16   { return s.servicePort }
func (s *SMTP) AuthName() string      { return s.authName }
func (s *SMTP) AuthPassword() string  { return s.authPassword }
func (s *SMTP) SenderAddress() string { return s.senderAddress }

type SMTPMapper struct {
	HostName      string `yaml:"host_name"`
	ServicePort   uint16 `yaml:"service_port"`
	AuthName      string `yaml:"auth_name"`
	AuthPassword  string `yaml:"auth_password"`
	SenderAddress string `yaml:"sender_address"`
}

func (m *SMTPMapper) Build() SMTP {
	return SMTP{
		hostName:      m.HostName,
		servicePort:   m.ServicePort,
		authName:      m.AuthName,
		authPassword:  m.AuthPassword,
		senderAddress: m.SenderAddress,
	}
}
