package quenv

import "time"

type Token struct {
	signingKey []byte
	accessExp  time.Duration
	refreshExp time.Duration
}

func (t *Token) SigningKey() []byte               { return append([]byte{}, t.signingKey...) }
func (t *Token) AccessExpiration() time.Duration  { return t.accessExp }
func (t *Token) RefreshExpiration() time.Duration { return t.refreshExp }

type TokenMapper struct {
	SigningKey         string `yaml:"signing_key"`
	AccessExpDuration  int64  `yaml:"access_exp_duration"`
	RefreshExpDuration int64  `yaml:"refresh_exp_duration"`
}

func (m *TokenMapper) Build() Token {
	return Token{
		signingKey: []byte(m.SigningKey),
		accessExp:  time.Duration(m.AccessExpDuration) * time.Second,
		refreshExp: time.Duration(m.RefreshExpDuration) * time.Second,
	}
}
