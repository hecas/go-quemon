package quenv

import (
	"strings"

	"github.com/labstack/gommon/log"
)

func LogLevel(s string) log.Lvl {
	switch strings.ToLower(s) {
	case "debug":
		return log.DEBUG
	case "info":
		return log.INFO
	case "warn":
		return log.WARN
	case "error":
		return log.ERROR
	}
	return log.OFF
}
