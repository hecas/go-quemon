package quenv

import "fmt"

type RabbitMQ struct {
	hostName     string
	servicePort  uint16
	authName     string
	authPassword string
}

func (r *RabbitMQ) HostName() string     { return r.hostName }
func (r *RabbitMQ) ServicePort() uint16  { return r.servicePort }
func (r *RabbitMQ) AuthName() string     { return r.authName }
func (r *RabbitMQ) AuthPassword() string { return r.authPassword }

func (r *RabbitMQ) ConnectionURL() string {
	return fmt.Sprintf("amqp://%s:%s@%s:%d/",
		r.authName,
		r.authPassword,
		r.hostName,
		r.servicePort,
	)
}

type RabbitMQMapper struct {
	HostName     string `yaml:"host_name"`
	ServicePort  uint16 `yaml:"service_port"`
	AuthName     string `yaml:"auth_name"`
	AuthPassword string `yaml:"auth_password"`
}

func (m *RabbitMQMapper) Build() RabbitMQ {
	return RabbitMQ{
		hostName:     m.HostName,
		servicePort:  m.ServicePort,
		authName:     m.AuthName,
		authPassword: m.AuthPassword,
	}
}
