package quenv

import "fmt"

type DB struct {
	typ          string
	hostName     string
	servicePort  uint16
	authName     string
	authPassword string
	dbName       string
	maxIdleConn  int
	maxOpenConn  int
	loggingSQL   bool
}

func (d *DB) Type() string            { return d.typ }
func (d *DB) HostName() string        { return d.hostName }
func (d *DB) ServicePort() uint16     { return d.servicePort }
func (d *DB) AuthName() string        { return d.authName }
func (d *DB) AuthPassword() string    { return d.authPassword }
func (d *DB) DBName() string          { return d.dbName }
func (d *DB) MaxIdleConnections() int { return d.maxIdleConn }
func (d *DB) MaxOpenConnections() int { return d.maxOpenConn }
func (d *DB) LoggingSQL() bool        { return d.loggingSQL }

func (d *DB) ConnectionString() string {
	switch d.typ {
	case "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
			d.authName,
			d.authPassword,
			d.hostName,
			d.servicePort,
			d.dbName,
		)
	case "godror":
		return fmt.Sprintf("%s/%s@//%s:%d/%s",
			d.authName,
			d.authPassword,
			d.hostName,
			d.servicePort,
			d.dbName,
		)
	}
	return ""
}

type DBMapper struct {
	Type               string `yaml:"type"`
	HostName           string `yaml:"host_name"`
	ServicePort        uint16 `yaml:"service_port"`
	AuthName           string `yaml:"auth_name"`
	AuthPassword       string `yaml:"auth_password"`
	DBName             string `yaml:"db_name"`
	MaxIdleConnections int    `yaml:"max_idle_connections"`
	MaxOpenConnections int    `yaml:"max_open_connections"`
	LoggingSQL         bool   `yaml:"log_sql"`
}

func (m *DBMapper) Build() DB {
	return DB{
		typ:          m.Type,
		hostName:     m.HostName,
		servicePort:  m.ServicePort,
		dbName:       m.DBName,
		authName:     m.AuthName,
		authPassword: m.AuthPassword,
		maxIdleConn:  m.MaxIdleConnections,
		maxOpenConn:  m.MaxOpenConnections,
		loggingSQL:   m.LoggingSQL,
	}
}
