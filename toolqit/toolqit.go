package toolqit

import (
	"bitbucket.org/hecas/go-quemon/toolqit/action"
	"github.com/urfave/cli/v2"
)

func Toolqit(modulePath string, args ...string) error {
	if modulePath == "" {
		modulePath = "bitbucket.org/hecas/go-quemon"
	}
	cliApp := &cli.App{
		Flags: []cli.Flag{
			&cli.PathFlag{
				Name:  "module",
				Value: modulePath,
				Usage: "모듈 경로",
			},
		},
		Commands: []*cli.Command{
			{
				Name:      "build",
				Usage:     "빌드합니다.",
				ArgsUsage: "[cmds...]",
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:  "d",
						Value: "/opt/qrsys/mate",
						Usage: "출력 될 디렉토리",
					},
				},
				Action: action.Build,
			},
			{
				Name:      "swagger",
				Usage:     "cmd/<cmd> 의 스웨거를 생성합니다.",
				ArgsUsage: "[cmds...]",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "fmt",
						Usage: "swagger 코드 정렬",
					},
				},
				Action: action.GenerateSwagger,
			},
			{
				Name:  "shape",
				Usage: "소스 파일을 생성합니다.",
				Subcommands: []*cli.Command{
					{
						Name:      "service",
						Usage:     "pkg/<service> 로 서비스 기본 구조를 생성합니다.",
						ArgsUsage: "<service>",
						Action:    action.ShapeService,
					},
					{
						Name:      "entity",
						Usage:     "pkg/<service>/model/<entity.go 로 엔티티를 생성합니다.",
						ArgsUsage: "<service> <entity>",
						Action:    action.ShapeEntity,
					},
				},
			},
		},
	}
	return cliApp.Run(args)
}
