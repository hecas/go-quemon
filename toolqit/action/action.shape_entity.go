package action

import (
	"fmt"

	"github.com/stoewer/go-strcase"
	"github.com/urfave/cli/v2"
)

func ShapeEntity(ctx *cli.Context) error {
	if ctx.NArg() < 1 {
		cli.ShowCommandHelpAndExit(ctx, "entity", 1)
	}
	module := ctx.Path("module")
	service := ctx.Args().Get(0)
	entity := ctx.Args().Get(1)
	return shapeEntity(module, service, entity)
}

func shapeEntity(module, service, entity string) error {
	param := map[any]any{
		"Module":         module,
		"Service":        service,
		"Entity":         strcase.UpperCamelCase(entity),
		"EntityReceiver": string([]rune(strcase.LowerCamelCase(entity))[0]),
		"EntityLower":    strcase.LowerCamelCase(entity),
	}
	err := overwriteTemplate(tplModelEntity, param,
		fmt.Sprintf("pkg/%s/model/model.%s.go", service, strcase.LowerCamelCase(entity)),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAppService, param,
		fmt.Sprintf("pkg/%s/app/app.%s_service.go", service, strcase.LowerCamelCase(entity)),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAppQuery, param,
		fmt.Sprintf("pkg/%s/app/app.%s_query.go", service, strcase.LowerCamelCase(entity)),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterInfraRepository, param,
		fmt.Sprintf("pkg/%s/adapter/infra/infra.%s_repository.go", service, strcase.LowerCamelCase(entity)),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterInfraQuery, param,
		fmt.Sprintf("pkg/%s/adapter/infra/infra.%s_query.go", service, strcase.LowerCamelCase(entity)),
	)
	if err != nil {
		return err
	}
	return nil
}

const tplModelEntity = `
package model

import (
	"time"

	"bitbucket.org/hecas/go-quemon/qernel/model"
	"github.com/google/uuid"
)

type {{ .Entity }}ID string

type {{ .Entity }}Repository interface {
	GetByID(id {{ .Entity }}ID) (*{{ .Entity }}, error)
	Save({{ .EntityLower }} *{{ .Entity }}) error
}

type {{ .Entity }} struct {
	entity       *model.EntityBase
	ID           {{ .Entity }}ID ` + "`xorm:\"varchar(48) 'id' pk\"`" + `
	Version      int             ` + "`xorm:\"int(11) 'version' notnull\"`" + `
	CreationTime time.Time       ` + "`xorm:\"datetime(3) notnull\"`" + `
}

func ({{ .EntityReceiver }} *{{ .Entity }}) TableName() string                { return "{{ .Service }}_{{ .EntityLower }}" }
func ({{ .EntityReceiver }} *{{ .Entity }}) GetID() string                    { return {{ .EntityReceiver }}.entity.GetID() }
func ({{ .EntityReceiver }} *{{ .Entity }}) GetRootVersion() int              { return {{ .EntityReceiver }}.entity.GetRootVersion() }
func ({{ .EntityReceiver }} *{{ .Entity }}) GetHistory() []*model.DomainEvent { return {{ .EntityReceiver }}.entity.GetHistory() }
func ({{ .EntityReceiver }} *{{ .Entity }}) RebaseHistory()                   { {{ .EntityReceiver }}.entity = model.NewEntityBase({{ .EntityReceiver }}, string({{ .EntityReceiver }}.ID), &{{ .EntityReceiver }}.Version) }

func New{{ .Entity }}() *{{ .Entity }} {
	{{ .EntityReceiver }} := {{ .Entity }}{ID: {{ .Entity }}ID(uuid.New().String())}
	{{ .EntityReceiver }}.RebaseHistory()
	{{ .EntityReceiver }}.entity.RaiseEvent(&{{ .Entity }}Created{
		CreationTime: time.Now(),
	})
	return &{{ .EntityReceiver }}
}

type {{ .Entity }}Created struct {
	CreationTime time.Time ` + "`json:\"creation_time\"`" + `
}

func (e *{{ .Entity }}Created) Class() string { return "{{ .Entity }}.Created" }
func (e *{{ .Entity }}Created) Apply(target *{{ .Entity }}) {
	target.CreationTime = e.CreationTime
}
`
const tplAppService = `
package app

import "{{ .Module }}/pkg/{{ .Service }}/model"

type {{ .Entity }}Service struct {
	{{ .EntityLower }}Repo model.{{ .Entity }}Repository
}

func New{{ .Entity }}Service(provider Provider) *{{ .Entity }}Service {
	return &{{ .Entity }}Service{
		{{ .EntityLower }}Repo: provider.{{ .Entity }}Repository(),
	}
}

func (s *{{ .Entity }}Service) Create() (string, error) {
	{{ .EntityLower }} := model.New{{ .Entity }}()
	err := s.{{ .EntityLower }}Repo.Save({{ .EntityLower }})
	if err != nil {
		return "", err
	}
	return string({{ .EntityLower }}.ID), nil
}
`
const tplAppQuery = `
package app

import "time"

type {{ .Entity }}QueryFilter struct {
	Offset  uint64
	Limit   uint
	IDIn    []string
	OrderBy []string
}

type {{ .Entity }}QueryItem struct {
	ID           string    ` + "`json:\"id\"`" + `
	CreationTime time.Time ` + "`json:\"creation_time\"`" + `
}

type {{ .Entity }}Query interface {
	TotalCount(filter *{{ .Entity }}QueryFilter) (uint64, error)
	Search(filter *{{ .Entity }}QueryFilter) ([]{{ .Entity }}QueryItem, error)
}
`
const tplAdapterInfraRepository = `
package infra

import (
	"{{ .Module }}/pkg/{{ .Service }}/model"
	"bitbucket.org/hecas/go-quemon/qernel/infra"
	"xorm.io/xorm"
)

type {{ .Entity }}Repository struct {
	r *infra.Repository[*model.{{ .Entity }}]
}

func New{{ .Entity }}Repository(provider Provider) *{{ .Entity }}Repository {
	return &{{ .Entity }}Repository{
		infra.NewRepository[*model.{{ .Entity }}](
			provider.XormSession(), 
			provider.EventStore(), 
			"{{ .Service }}_{{ .EntityLower }}_event",
			func() *model.{{ .Entity }} {
				return &model.{{ .Entity }}{}
			},
		),
	}
}

func (r *{{ .Entity }}Repository) GetByID(id model.{{ .Entity}}ID) (*model.{{ .Entity }}, error) {
	return r.r.GetEntity(func(xormSess *xorm.Session) {
		xormSess.ID(id)
	})
}

func (r *{{ .Entity }}Repository) Save(entity *model.{{ .Entity }}) error {
	return r.r.Save(entity)
}
`
const tplAdapterInfraQuery = `
package infra

import (
	"strings"
	
	"{{ .Module }}/pkg/{{ .Service }}/app"
	"bitbucket.org/hecas/go-quemon/rdbutils"
	"github.com/pkg/errors"
	"xorm.io/builder"
	"xorm.io/xorm"
)

type {{ .Entity }}Query struct {
	xormSess *xorm.Session
}

func New{{ .Entity }}Query(provider Provider) *{{ .Entity }}Query {
	return &{{ .Entity }}Query{
		xormSess: provider.XormSession(),
	}
}

func (s *{{ .Entity }}Query) baseSQL(filter *app.{{ .Entity }}QueryFilter) *builder.Builder {
	return builder.MySQL().
		From("{{ .Service}}_{{ .EntityLower }}").
		Select("id", "creation_time").
		Where(builder.And(
			rdbutils.Cond.In("id", filter.IDIn),
		))
}

func (s *{{ .Entity }}Query) TotalCount(filter *app.{{ .Entity }}QueryFilter) (uint64, error) {
	baseSQL := s.baseSQL(filter)
	countSQL := builder.MySQL().
		From(baseSQL, "r").
		Select("COUNT(*) cnt")
	rs, err := rdbutils.Query(s.xormSess, countSQL)
	if err != nil {
		return 0, errors.WithStack(err)
	}
	return rs[0].UInt64("cnt"), nil
}

func (s *{{ .Entity }}Query) Search(filter *app.{{ .Entity }}QueryFilter) ([]app.{{ .Entity }}QueryItem, error) {
	baseSQL := s.baseSQL(filter)
	searchSQL := builder.MySQL().
		From(baseSQL, "r").
		Select("*").
		OrderBy(strings.TrimSpace(strings.Join(filter.OrderBy, ",")))
	if filter.Limit > 0 {
		searchSQL.Limit(int(filter.Limit), int(filter.Offset))
	}
	rs, err := rdbutils.Query(s.xormSess, searchSQL)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	items := []app.{{ .Entity }}QueryItem{}
	for i := range rs {
		item := app.{{ .Entity }}QueryItem{
			ID:           rs.String(i, "id"),
			CreationTime: rs.Time(i, "creation_time"),
		}
		items = append(items, item)
	}
	return items, nil
}
`
