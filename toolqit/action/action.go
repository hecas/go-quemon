package action

import (
	"os"
	"os/exec"
	"path"
	"strings"
	"text/template"

	"github.com/urfave/cli/v2"
)

func Build(ctx *cli.Context) error {
	dir := ctx.String("d")
	err := os.MkdirAll(dir, 0755)
	if err != nil {
		return err
	}
	for i := 0; i < ctx.NArg(); i++ {
		target := ctx.Args().Get(i)
		execCmd := exec.Command(
			"go", "build",
			"-o", path.Join(dir, target),
			path.Join("cmd", target, "main.go"),
		)
		err := execCmd.Run()
		if err != nil {
			return err
		}
	}
	return nil
}

func GenerateSwagger(ctx *cli.Context) error {
	oldDir, err := os.Getwd()
	if err != nil {
		return err
	}
	defer os.Chdir(oldDir)
	usingFormat := ctx.Bool("fmt")
	for i := 0; i < ctx.NArg(); i++ {
		target := ctx.Args().Get(i)
		execCmd := exec.Command(
			"swag", "init",
			"-d", path.Join("cmd", target),
			"-g", "main.go",
			"--parseDependency",
			"--parseDepth", "4",
			"-o", path.Join("cmd", target, "docs"),
		)
		err := execCmd.Run()
		if err != nil {
			return err
		}
	}
	if usingFormat {
		execCmd := exec.Command(
			"swag", "fmt",
		)
		err := execCmd.Run()
		if err != nil {
			return err
		}
	}
	return nil
}

func overwriteTemplate(tplText string, v interface{}, filepath string) error {
	err := os.MkdirAll(path.Dir(filepath), 0755)
	if err != nil {
		return err
	}
	f, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer f.Close()
	tpl, err := template.New("").Parse(strings.TrimSpace(tplText) + "\n")
	if err != nil {
		return err
	}
	err = tpl.Execute(f, v)
	if err != nil {
		return err
	}
	execCmd := exec.Command(
		"gofmt", "-w", filepath,
	)
	return execCmd.Run()
}
