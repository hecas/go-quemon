package action

import (
	"fmt"

	"github.com/stoewer/go-strcase"
	"github.com/urfave/cli/v2"
)

func ShapeService(ctx *cli.Context) error {
	if ctx.NArg() < 1 {
		cli.ShowCommandHelpAndExit(ctx, "service", 1)
	}
	module := ctx.Path("module")
	service := ctx.Args().Get(0)
	return shapeService(module, service)
}

func shapeService(module, service string) error {
	entity := service
	err := shapeEntity(module, service, entity)
	if err != nil {
		return err
	}
	param := map[any]any{
		"Module":      module,
		"Service":     service,
		"Entity":      strcase.UpperCamelCase(entity),
		"EntityLower": strcase.LowerCamelCase(entity),
	}
	err = overwriteTemplate(tplModel, param,
		fmt.Sprintf("pkg/%s/model/model.go", service))
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplApp, param,
		fmt.Sprintf("pkg/%s/app/app.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAppAuthService, param,
		fmt.Sprintf("pkg/%s/app/app.auth_service.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterInfra, param,
		fmt.Sprintf("pkg/%s/adapter/infra/infra.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterInfraAuthTokenService, param,
		fmt.Sprintf("pkg/%s/adapter/infra/infra.auth_token_service.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterController, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterControllerCommand, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.command.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterControllerSetup, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.setup.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterControllerHTTP, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.http.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterControllerWS, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.ws.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterControllerSession, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.session.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterControllerEvent, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.event.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplAdapterControllerTask, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.task.go", service),
	)
	if err != nil {
		return err
	}
	err = overwriteTemplate(tplCmdMain, param,
		fmt.Sprintf("cmd/svc-%s/main.go", service),
	)
	if err != nil {
		return err
	}
	return nil
}

const tplModel = `
package model

type UserID string
`

const tplApp = `
package app

import (
	"{{ .Module }}/pkg/{{ .Service }}/model"
)

type Provider interface {
	Begin() error
	Commit() error
	Close() error

	AuthTokenService() AuthTokenService
	{{ .Entity }}Query() {{ .Entity }}Query

	{{ .Entity }}Repository() model.{{ .Entity }}Repository
}
`
const tplAdapterInfra = `
package infra

import (
	"bitbucket.org/hecas/go-quemon/qernel/infra"
	"bitbucket.org/hecas/go-quemon/quenv"
	"github.com/go-redis/redis/v8"
	"xorm.io/xorm"
)

type Provider interface {
	XormSession() *xorm.Session
	Redis() *redis.Client
	Token() quenv.Token
	EventStore() *infra.EventStore
}
`
const tplAdapterInfraAuthTokenService = `
package infra

import (
	"{{ .Module }}/pkg/{{ .Service }}/app"
	"bitbucket.org/hecas/go-quemon/quenv"
	"bitbucket.org/hecas/go-quemon/querr"
	"github.com/golang-jwt/jwt"
)

type AuthTokenService struct {
	token quenv.Token
}

func NewAuthTokenService(provider Provider) *AuthTokenService {
	return &AuthTokenService{
		token: provider.Token(),
	}
}

func (s *AuthTokenService) VerifyToken(token string) (*app.AuthClaims, error) {
	parser := jwt.Parser{
		ValidMethods:         []string{jwt.SigningMethodHS256.Name},
		UseJSONNumber:        true,
		SkipClaimsValidation: true,
	}
	claims := app.AuthClaims{}
	_, err := parser.ParseWithClaims(token, &claims, func(t *jwt.Token) (interface{}, error) {
		return s.token.SigningKey(), nil
	})
	if err != nil {
		return nil, querr.MsgErrBadRequest("접근토큰이 유효하지 않습니다.")
	}
	return &claims, nil
}
`
const tplAppAuthService = `
package app

import (
	"{{ .Module }}/pkg/{{ .Service }}/model"
	"bitbucket.org/hecas/go-quemon/shortcut"
	"github.com/golang-jwt/jwt"
)

const (
	PermissionManageService = "manage service"
)

type AuthClaims struct {
	jwt.StandardClaims
	User struct {
		ID   string    ` + "`json:\"user_id\"`" + `
		Role struct {
			Name        string   ` + "`json:\"name\"`" + `
			Level       int      ` + "`json:\"level\"`" + `
			Permissions []string ` + "`json:\"permissions\"`" + `
		} ` + "`json:\"role\"`" + `
	} ` + "`json:\"user\"`" + `
}

func (a *AuthClaims) UserID() model.UserID {
	return model.UserID(a.User.ID)
}

func (a *AuthClaims) HasManageServicePermission() bool {
	return shortcut.HasSomeInSlice(a.User.Role.Permissions, PermissionManageService)
}

type AuthTokenService interface {
	VerifyToken(token string) (*AuthClaims, error)
}

type AuthService struct {
	tokenService AuthTokenService
}

func NewAuthService(provider Provider) *AuthService {
	return &AuthService{
		tokenService: provider.AuthTokenService(),
	}
}

func (s *AuthService) VerifyAccessToken(token string) (*AuthClaims, error) {
	authClaims, err := s.tokenService.VerifyToken(token)
	if err != nil {
		return nil, err
	}
	return authClaims, nil
}
`
const tplAdapterController = `
package controller

import (
	"{{ .Module }}/pkg/{{ .Service }}/adapter/infra"
	"{{ .Module }}/pkg/{{ .Service }}/app"
	"{{ .Module }}/pkg/{{ .Service }}/model"
	qinfra "bitbucket.org/hecas/go-quemon/qernel/infra"
	"bitbucket.org/hecas/go-quemon/quenv"
	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	"github.com/streadway/amqp"
	"xorm.io/xorm"
)

type Config struct {
	Echo         *echo.Echo
	Xorm         *xorm.Engine
	Redis        *redis.Client
	AMQP         *amqp.Connection
	Token        quenv.Token
	EventManager *qinfra.EventManager
	TaskSet      *qinfra.TaskSet
}

type appProvider struct {
	config     *Config
	xormSess   *xorm.Session
	eventStore *qinfra.EventStore
}

func newAppProvider(config *Config) *appProvider {
	return &appProvider{
		config: config,
	}
}

var _ app.Provider = &appProvider{}
var _ infra.Provider = &appProvider{}

func (c *appProvider) Begin() error {
	err := c.XormSession().Begin()
	return err
}

func (c *appProvider) Commit() error {
	err := c.XormSession().Commit()
	stores := c.EventStore().AppendedStore()
	c.config.EventManager.RequestPublish(stores...)
	return err
}

func (c *appProvider) Close() error {
	err := c.XormSession().Close()
	c.xormSess = nil
	c.eventStore = nil
	return err
}

func (c *appProvider) AuthTokenService() app.AuthTokenService {
	return infra.NewAuthTokenService(c)
}

func (c *appProvider) {{ .Entity }}Query() app.{{ .Entity }}Query {
	return infra.New{{ .Entity }}Query(c)
}

func (c *appProvider) {{ .Entity }}Repository() model.{{ .Entity }}Repository {
	return infra.New{{ .Entity }}Repository(c)
}

func (c *appProvider) XormSession() *xorm.Session {
	if c.xormSess == nil {
		c.xormSess = c.config.Xorm.NewSession()
	}
	return c.xormSess
}

func (c *appProvider) Redis() *redis.Client {
	return c.config.Redis
}

func (c *appProvider) Token() quenv.Token {
	return c.config.Token
}

func (c *appProvider) EventStore() *qinfra.EventStore {
	if c.eventStore == nil {
		c.eventStore = qinfra.NewEventStore(c.XormSession())
	}
	return c.eventStore
}
`
const tplAdapterControllerCommand = `
package controller

import (
	"bitbucket.org/hecas/go-quemon/msgerr"
	"{{ .Module }}/pkg/{{ .Service }}/app"
)

type simpleMessage struct {
	msgerr.Descriptor
	Result struct{} ` + "`json:\"result\"`" + `
}

// setAuth
type setAuthCommand struct {
	AccessToken string ` + "`json:\"access_token\"`" + `
}

type setAuthMessage struct {
	msgerr.Descriptor
	Result *app.AuthClaims ` + "`json:\"result\"`" + `
}

// restoreSession
type restoreSessionCommand struct {
	SessionID string ` + "`json:\"session_id\"`" + `
}

type restoreSessionMessage simpleMessage

// create{{ .Entity }}
type create{{ .Entity }}Command struct {}

type create{{ .Entity }}Message struct {
	msgerr.Descriptor
	Result *create{{ .Entity }}Result ` + "`json:\"result\"`" + `
}

type create{{ .Entity }}Result struct {
	ID string ` + "`json:\"id\"`" + `
}

// search{{ .Entity }}
type search{{ .Entity }}Message struct {
	msgerr.Descriptor
	Result *search{{ .Entity }}Result ` + "`json:\"result\"`" + `
}

type search{{ .Entity }}Result struct {
	TotalCount uint64 ` + "`json:\"total_count\"`" + `
	FoundCount int ` + "`json:\"found_count\"`" + `
	Items      []app.{{ .Entity }}QueryItem ` + "`json:\"items\"`" + `
}
`
const tplAdapterControllerHTTP = `
package controller

import (
	"net/http"

	"{{ .Module }}/pkg/{{ .Service }}/app"
	"bitbucket.org/hecas/go-quemon/querr"
	"github.com/labstack/echo/v4"
)

type httpHandler handlerBase

func (h httpHandler) callApp(begin bool, call func(appProvider app.Provider) error) error {
	appProvider := newAppProvider(h.config)
	defer appProvider.Close()
	if begin {
		appProvider.Begin()
	}
	err := call(appProvider)
	if err != nil {
		return err
	}
	if begin {
		appProvider.Commit()
	}
	return nil
}

// @Tags     {{ .Service }}.v1
// @Router   /api/{{ .Service }}/v1/{{ .EntityLower }}s [post]
// @Summary  {{ .Entity }} 생성
// @Security JWT
// @Accept   json
// @Param    request_body body create{{ .Entity }}Command true "요청"
// @Produce  json
// @Success  201 {object} create{{ .Entity }}Message "생성됨"
func (h httpHandler) create{{ .Entity }}(ctx echo.Context) error {
	var cmd create{{ .Entity }}Command
	err := ctx.Bind(&cmd)
	if err != nil {
		return err
	}
	return h.callApp(true, func(appProvider app.Provider) error {
		service := app.New{{ .Entity }}Service(appProvider)
		id, err := service.Create()
		if err != nil {
			return err
		}
		return ctx.JSON(http.StatusCreated, create{{ .Entity }}Message{
			Descriptor: querr.MsgDescriptorOK("생성되었습니다."),
			Result: &create{{ .Entity }}Result {
				ID: id,
			},
		})
	})
}

// @Tags     {{ .Service }}.v1
// @Router   /api/{{ .Service }}/v1/{{ .EntityLower }}s [get]
// @Summary  {{ .Entity }} 조회
// @Security JWT
// @Param    offset   query int      false "offset" default(0)  minimum(0)
// @Param    limit    query int      false "limit"  default(20) minimum(1) maximum(100)
// @Param    id       query []string false "{{ .Entity }} id" collectionFormat(multi)
// @Produce  json
// @Success  200 {object} search{{ .Entity }}Message
func (h httpHandler) search{{ .Entity }}(ctx echo.Context) error {
	var filter app.{{ .Entity }}QueryFilter
	err := echo.QueryParamsBinder(ctx).
		Uint64("offset", &filter.Offset).
		Uint("limit", &filter.Limit).
		Strings("id", &filter.IDIn).
		Strings("orderby", &filter.OrderBy).
		BindError()
	if err != nil {
		return err
	}
	if filter.Limit > 100 {
		filter.Limit = 100
	}
	return h.callApp(false, func(appProvider app.Provider) error {
		totalCount, err := appProvider.{{ .Entity }}Query().TotalCount(&filter)
		if err != nil {
			return err
		}
		items, err := appProvider.{{ .Entity }}Query().Search(&filter)
		if err != nil {
			return err
		}
		return ctx.JSON(http.StatusOK, search{{ .Entity }}Message{
			Descriptor: querr.MsgDescriptorOK("조회되었습니다."),
			Result: &search{{ .Entity }}Result{
				TotalCount: totalCount,
				FoundCount: len(items),
				Items:      items,
			},
		})
	})
}
`
const tplAdapterControllerSetup = `
package controller

import (
	"bitbucket.org/hecas/go-quemon/qernel/controller"
	"bitbucket.org/hecas/go-quemon/qernel/infra"
	"github.com/labstack/echo/v4"
)

func Setup(config *Config) error {
	base := handlerBase{
		config:    config,
		wsConnMgr: infra.NewLocalObjectManager[*controller.WSConnection](),
	}
	syncDB(base)
	setupHTTP(base)
	setupWS(base)
	setupEvent(base)
	setupTask(base)
	return nil
}

type handlerBase struct {
	config    *Config
	wsConnMgr *infra.LocalObjectManager[*controller.WSConnection]
}

func syncDB(base handlerBase) {
	base.config.Xorm.Sync2()
}

func setupHTTP(base handlerBase) {
	h := httpHandler(base)
	controller.SetupHTTPHandler(base.config.Echo, []controller.HTTPHandler{
		{Methods: []string{echo.POST}, Path: "/api/{{ .Service }}/v1/{{ .EntityLower }}s", Handle: h.create{{ .Entity }}},
		{Methods: []string{echo.GET}, Path: "/api/{{ .Service }}/v1/{{ .EntityLower }}s", Handle: h.search{{ .Entity }}},
	})
}

func setupWS(base handlerBase) {
	h := wsHandler(base)
	controller.SetupWSHandler(base.config.Echo, []controller.WSHandler{
		{
			Path:        "/api/{{ .Service }}/v1/websocket",
			OnConnected: h.handleConnected,
			OnClosed:    h.handleClosed,
			OnRead: controller.SimpleWSReadHandler(controller.SimpleWSRouter{
				"SetAuth": h.setAuth,
			}),
		},
	})
}

func setupEvent(base handlerBase) {
	controller.SetupTaskHandler(base.config.TaskSet, []controller.TaskHandler{
		{
			Name: "event.publish",
			Handle: controller.PublishEventTask(base.config.EventManager, []string{
				"{{ .Service }}_{{ .EntityLower }}_event",
			}),
		},
		{
			Name: "event.subscribe",
			Handle: controller.SubscribeEventTask(base.config.EventManager, func() []controller.EventSubscription {
				h := eventHandler(base)
				return []controller.EventSubscription{
					{
						Key:    "{{ .Entity }}.#",
						Router: controller.EventRouter{
							"{{ .Entity }}.{{ .Entity }}Created": h.{{ .EntityLower }}Created,
						},
					},
				}
			}()),
		},
	})
}

func setupTask(base handlerBase) {
	h := taskHandler(base)
	controller.SetupTaskHandler(base.config.TaskSet, []controller.TaskHandler{
		{Name: "task.quack", Handle: h.quack},
	})
}
`
const tplAdapterControllerWS = `
package controller

import (
	"net/http"

	"{{ .Module }}/pkg/{{ .Service }}/app"
	"bitbucket.org/hecas/go-quemon/qernel/controller"
	"bitbucket.org/hecas/go-quemon/querr"
	"github.com/google/uuid"
)

type wsHandler handlerBase

func (h wsHandler) callApp(begin bool, call func(appProvider app.Provider) error) error {
	appProvider := newAppProvider(h.config)
	defer appProvider.Close()
	if begin {
		appProvider.Begin()
	}
	err := call(appProvider)
	if err != nil {
		return err
	}
	if begin {
		appProvider.Commit()
	}
	return nil
}

func (h wsHandler) session(req *controller.SimpleWSRequest) *session {
	conn := req.Connection()
	return conn.Get("session").(*session)
}

func (h wsHandler) handleConnected(conn *controller.WSConnection) {
	h.wsConnMgr.AcquireObject(conn.ID(), nil)
	sess := newSession(uuid.New().String())
	conn.Set("session", sess)
	controller.SimpleWSNotify(conn, "@Session.Connected", &controller.SimpleWSResponse{
		Descriptor: querr.MsgDescriptorOK("접속되었습니다."),
		Result: map[string]any{
			"session_id": sess.id,
		},
	})
}

func (h wsHandler) handleClosed(conn *controller.WSConnection) {
	h.wsConnMgr.ReleaseObject(conn.ID(), true)
}

func (h wsHandler) setAuth(req *controller.SimpleWSRequest) error {
	var cmd setAuthCommand
	err := req.Bind(&cmd)
	if err != nil {
		return err
	}
	sess := h.session(req)
	return h.callApp(true, func(appProvider app.Provider) error {
		claims, err := app.NewAuthService(appProvider).
			VerifyAccessToken(cmd.AccessToken)
		if err != nil {
			return err
		}
		sess.setAuth(claims)
		msg := setAuthMessage{
			Descriptor: querr.MsgDescriptorOK("인증되었습니다"),
			Result:     claims,
		}
		return req.Response(controller.NewSimpleWSResponse(http.StatusOK, msg.Descriptor, msg.Result))
	})
}
`
const tplAdapterControllerSession = `
package controller

import (
	"{{ .Module }}/pkg/{{ .Service }}/app"
)

type session struct {
	id     string
	auth   *app.AuthClaims
}

func newSession(id string) *session {
	return &session{id: id}
}

func (s *session) ID() string { return s.id }

func (s *session) setAuth(auth *app.AuthClaims) {
	s.auth = auth
}
`
const tplAdapterControllerEvent = `
package controller

import (
	"encoding/json"
	"time"
)

type eventHandler handlerBase

func (h eventHandler) {{ .EntityLower }}Created(key string, data []byte) error {
	var evt struct {
		ID        string    ` + "`json:\"id\"`" + `
		FiredTime time.Time ` + "`json:\"fired_time\"`" + `
		Payload   struct {
		} ` + "`json:\"payload\"`" + `
	}
	err := json.Unmarshal(data, &evt)
	if err != nil {
		return err
	}
	return nil
}
`
const tplAdapterControllerTask = `
package controller

import (
	"context"
	"time"

	"github.com/labstack/gommon/log"
)

type taskHandler handlerBase

func (h taskHandler) quack(ctx context.Context) error {
	period := 5 * time.Minute
	handleOnTime := func(at time.Time) {
		log.Debug("Quack!")
	}
	timer := time.NewTimer(period)
	defer timer.Stop()
	for {
		select {
		case <-ctx.Done():
			return nil
		case now := <-timer.C:
			handleOnTime(now)
			timer.Reset(period)
		}
	}
}
`
const tplCmdMain = `
package main

import (
	"context"
	"html/template"
	"io"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "{{ .Module }}/cmd/svc-{{ .Service }}/docs"
	_ "github.com/go-sql-driver/mysql"

	"bitbucket.org/hecas/go-quemon/msgerr"
	"bitbucket.org/hecas/go-quemon/qernel/infra"
	"bitbucket.org/hecas/go-quemon/quenv"
	"bitbucket.org/hecas/go-quemon/querr"
	"{{ .Module }}/pkg/{{ .Service }}/adapter/controller"
	"github.com/davecgh/go-spew/spew"
	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	echoSwagger "github.com/swaggo/echo-swagger"
	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v2"
	"xorm.io/core"
	"xorm.io/xorm"
)

// @title                      {{ .Service }} API
// @version                    1.0
// @description                {{ .Service }} API 입니다.
// @contact.name               Querensys
// @contact.email              madcoder@querensys.com
// @BasePath                   /
// @securityDefinitions.apikey JWT
// @in                         header
// @name                       Authorization
func main() {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	cliApp := &cli.App{
		Flags: []cli.Flag{
			&cli.PathFlag{
				Name:  "w",
				Value: wd,
				Usage: "작업 디렉토리",
			},
		},
		Commands: []*cli.Command{
			{
				Name:  "run",
				Usage: "서비스를 실행합니다.",
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:  "conf",
						Value: "conf/{{ .Service }}.yml",
						Usage: "설정파일",
					},
				},
				Action: RunApplication,
			},
		},
	}
	err = cliApp.Run(os.Args)
	if err != nil {
		log.Fatalf("%+v", err)
	}
}

type Application struct {
	LogLevel      log.Lvl
	ListenAddress string
	Token         quenv.Token
	MariaDB       quenv.DB
	Redis         quenv.Redis
	RabbitMQ      quenv.RabbitMQ
	SMTP          quenv.SMTP
}

func RunApplication(ctx *cli.Context) error {
	wd := ctx.Path("w")
	err := os.Chdir(wd)
	if err != nil {
		return errors.WithStack(err)
	}
	ap, err := func() (*Application, error) {
		f, err := os.Open(ctx.Path("conf"))
		if err != nil {
			return nil, errors.WithStack(err)
		}
		defer f.Close()
		var ap Application
		err = yaml.NewDecoder(f).Decode(&ap)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		return &ap, nil
	}()
	if err != nil {
		return err
	}
	return ap.Run()
}

func (a *Application) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var mapper struct {
		LogLevel      string               ` + "`yaml:\"log_level\"`" + `
		ListenAddress string               ` + "`yaml:\"listen_address\"`" + `
		Token         quenv.TokenMapper    ` + "`yaml:\"token\"`" + `
		MariaDB       quenv.DBMapper       ` + "`yaml:\"mariadb\"`" + `
		Redis         quenv.RedisMapper    ` + "`yaml:\"redis\"`" + `
		RabbitMQ      quenv.RabbitMQMapper ` + "`yaml:\"rabbitmq\"`" + `
		SMTP          quenv.SMTPMapper     ` + "`yaml:\"smtp\"`" + `
	}
	err := unmarshal(&mapper)
	if err != nil {
		return errors.WithStack(err)
	}
	a.LogLevel = quenv.LogLevel(mapper.LogLevel)
	a.ListenAddress = mapper.ListenAddress
	a.Token = mapper.Token.Build()
	a.MariaDB = mapper.MariaDB.Build()
	a.Redis = mapper.Redis.Build()
	a.RabbitMQ = mapper.RabbitMQ.Build()
	a.SMTP = mapper.SMTP.Build()
	return nil
}

func (a *Application) Run() error {
	log.Infof("loglevel: %d", a.LogLevel)
	log.SetLevel(a.LogLevel)
	log.Info(spew.Sdump(a))
	xormEngn, err := xorm.NewEngine(a.MariaDB.Type(), a.MariaDB.ConnectionString())
	if err != nil {
		return errors.WithStack(err)
	}
	defer func() {
		log.Info("MariaDB closing")
		xormEngn.Close()
	}()
	xormEngn.SetMaxIdleConns(a.MariaDB.MaxIdleConnections())
	xormEngn.SetMaxOpenConns(a.MariaDB.MaxOpenConnections())
	xormEngn.SetMapper(core.GonicMapper{})
	xormEngn.ShowSQL(a.MariaDB.LoggingSQL())
	xormEngn.SetTZDatabase(time.UTC)

	redisClt := redis.NewClient(&redis.Options{
		Addr:     a.Redis.Address(),
		Password: a.Redis.AuthPassword(),
		DB:       a.Redis.Index(),
	})
	err = redisClt.Ping(context.Background()).Err()
	if err != nil {
		return errors.WithStack(err)
	}
	defer func() {
		log.Info("Redis closing")
		redisClt.Close()
	}()

	amqpConn, err := amqp.Dial(a.RabbitMQ.ConnectionURL())
	if err != nil {
		return errors.WithStack(err)
	}
	defer func() {
		log.Info("AMQP closing")
		amqpConn.Close()
	}()
	AMQClosed := amqpConn.NotifyClose(make(chan *amqp.Error))

	ec := echo.New()
	ec.Use(middleware.Logger())
	ec.Use(middleware.CORS())
	ec.HTTPErrorHandler = func(err error, ctx echo.Context) {
		log.Errorf("%+v", err)
		ec.DefaultHTTPErrorHandler(func() error {
			switch e := err.(type) {
			case *echo.HTTPError:
				msg, _ := e.Message.(string)
				return &echo.HTTPError{
					Code:    e.Code,
					Message: msgerr.NewWith(querr.FromHTTP(e.Code), msg),
				}
			case *msgerr.Error:
				return &echo.HTTPError{
					Code:    querr.HTTPCode(e),
					Message: e,
				}
			default:
				return err
			}
		}(), ctx)
	}
	ec.Renderer = &echoTemplateRenderer{}
	ec.GET("/api/{{ .Service }}/swagger/*", echoSwagger.EchoWrapHandler())

	eventManager := infra.NewEventManager(amqpConn, xormEngn, "user")
	taskSet := infra.TaskSet{}

	config := controller.Config{
		Echo:         ec,
		Xorm:         xormEngn,
		Redis:        redisClt,
		AMQP:         amqpConn,
		Token:        a.Token,
		EventManager: eventManager,
		TaskSet:      &taskSet,
	}
	err = controller.Setup(&config)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithCancel(context.Background())
	taskSet.Go(ctx, func(name string) {
		log.Error(name)
		cancel()
	})
	echoErrCh := make(chan error)
	go func() {
		err := ec.Start(a.ListenAddress)
		if err != nil {
			echoErrCh <- err
		}
	}()
	defer func() {
		log.Info("Echo closing")
		ec.Close()
	}()
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	select {
	case sig := <-sigCh:
		return errors.New(sig.String())
	case err := <-AMQClosed:
		return errors.WithStack(err)
	case err := <-echoErrCh:
		return errors.WithStack(err)
	}
}

type echoTemplateRenderer struct{}

func (*echoTemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	templ, err := template.ParseFiles(name)
	if err != nil {
		return err
	}
	return templ.ExecuteTemplate(w, templ.Name(), data)
}
`
