package shortcut

const (
	True  = true
	False = false
)

type MapByString map[string]any
type SetByString map[string]struct{}

func ToInterface(v any) any { return v }

func HasSomeInSlice[T comparable](in []T, items ...T) bool {
	for _, item := range items {
		for i := range in {
			if in[i] == item {
				return true
			}
		}
	}
	return false
}

func HasEveryInSlice[T comparable](in []T, items ...T) bool {
	for _, item := range items {
		found := false
		for i := range in {
			if in[i] == item {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}

func ConvertSlice[T any, R any](source []T, convert func(T) R) (result []R) {
	for i := range source {
		result = append(result, convert(source[i]))
	}
	return result
}
