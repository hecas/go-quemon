package queenfra

import (
	"math/rand"
	"time"

	"github.com/labstack/gommon/log"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"xorm.io/builder"
	"xorm.io/xorm"
)

type EventMessage struct {
	ID       int64     `xorm:"bigint(20) 'id' pk autoincr"`
	Exchange string    `xorm:"varchar(80) 'exchange'"`
	Key      string    `xorm:"varchar(80) 'key'"`
	Payload  string    `xorm:"text 'payload'"`
	Status   string    `xorm:"varchar(20) 'status'"`
	TryTime  time.Time `xorm:"datetime(3) 'try_time'"`
	TryCount int       `xorm:"int(11) 'try_count'"`
}

type EventOutbox struct {
	xormSess     *xorm.Session
	tableName    string
	exchangeName string
}

func NewEventOutbox(xormSess *xorm.Session, tableName string, exchangeName string) *EventOutbox {
	return &EventOutbox{
		xormSess:     xormSess,
		tableName:    tableName,
		exchangeName: exchangeName,
	}
}

func (p *EventOutbox) Save(key string, data []byte) error {
	_, err := p.xormSess.Table(p.tableName).Insert(&EventMessage{
		Exchange: p.exchangeName,
		Key:      key,
		Payload:  string(data),
		Status:   "trying",
		TryTime:  time.Now(),
	})
	if err != nil {
		return err
	}

	return nil
}

type EventPublisher struct {
	xormEngn     *xorm.Engine
	tableName    string
	exchangeName string

	amqpChan     *amqp.Channel
	sigDone      chan struct{}
	slotDuration time.Duration
}

func NewEventPublisher(
	xormEngn *xorm.Engine, tableName string,
	amqpConn *amqp.Connection, exchangeName string,
) (*EventPublisher, error) {
	err := xormEngn.Table(tableName).Sync2(&EventMessage{})
	if err != nil {
		return nil, errors.Wrap(err, "xorm sync failed")
	}

	amqpChan, err := amqpConn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "amqp channel failed")
	}

	err = amqpChan.ExchangeDeclare(exchangeName, "topic", true, false, false, false, nil)
	if err != nil {
		amqpChan.Close()

		return nil, errors.Wrap(err, "amqp exchange failed")
	}

	return &EventPublisher{
		xormEngn:     xormEngn,
		tableName:    tableName,
		exchangeName: exchangeName,

		amqpChan:     amqpChan,
		sigDone:      make(chan struct{}),
		slotDuration: 200 * time.Millisecond,
	}, nil
}

func (p *EventPublisher) Run() {
	timer := time.NewTimer(0)
	defer timer.Stop()

	for {
		select {
		case <-p.sigDone:
			p.amqpChan.Close()
			return
		case now := <-timer.C:
			err := p.onTime(now)
			if err != nil {
				log.Error(err)
			}

			timer.Reset(p.slotDuration)
		}
	}
}

func (p *EventPublisher) Stop() {
	close(p.sigDone)
}

func (p *EventPublisher) onTime(at time.Time) error {
	xormSess := p.xormEngn.NewSession()
	defer xormSess.Close()

	xormSess.Begin()

	msgs := []EventMessage{}
	err := xormSess.Table(p.tableName).
		Where(builder.And(
			builder.Eq{"exchange": p.exchangeName},
			builder.In("status", "trying"),
			builder.Lte{"try_time": at},
		)).
		OrderBy("try_time ASC").
		Limit(1000, 0).
		ForUpdate().
		Find(&msgs)
	if err != nil {
		return errors.Wrap(err, "xorm find failed")
	}

	for i := range msgs {
		msgs[i].TryCount++

		err := p.amqpChan.Publish(p.exchangeName, msgs[i].Key, true, false, amqp.Publishing{
			Body: []byte(msgs[i].Payload),
		})
		if err != nil {
			if msgs[i].TryCount > 10 {
				msgs[i].Status = "failed"
			} else {
				waitMs := rand.Int63n(p.slotDuration.Milliseconds() * int64(msgs[i].TryCount))
				msgs[i].TryTime = at.Add(time.Duration(waitMs) * time.Millisecond)
			}
		} else {
			msgs[i].Status = "completed"
		}

		_, err = xormSess.Table(p.tableName).ID(msgs[i].ID).Update(msgs[i])
		if err != nil {
			return errors.Wrap(err, "xorm update failed")
		}
	}

	xormSess.Commit()

	return nil
}

type EventSubscriber struct {
	onReadData func(key string, data []byte)

	amqpChan *amqp.Channel
	sigRead  <-chan amqp.Delivery
	sigDone  chan struct{}
}

func NewEventSubscriber(
	amqpConn *amqp.Connection, exchangeName string, queueName string,
	keys []string, onReadData func(key string, data []byte),
) (*EventSubscriber, error) {
	amqpChan, err := amqpConn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "amqp channel failed")
	}

	fail := func(err error) (*EventSubscriber, error) {
		amqpChan.Close()
		return nil, err
	}

	err = amqpChan.ExchangeDeclare(exchangeName, "topic", true, false, false, false, nil)
	if err != nil {
		return fail(errors.Wrap(err, "amqp exchange failed"))
	}

	durable := queueName != ""
	autoDelete := queueName == ""
	exclusive := queueName == ""

	amqpQueue, err := amqpChan.QueueDeclare(queueName, durable, autoDelete, exclusive, false, nil)
	if err != nil {
		return fail(errors.Wrap(err, "amqp queue failed"))
	}

	for i := range keys {
		err = amqpChan.QueueBind(amqpQueue.Name, keys[i], exchangeName, false, nil)
		if err != nil {
			return fail(errors.Wrap(err, "amqp bind failed"))
		}
	}

	sigRead, err := amqpChan.Consume(amqpQueue.Name, "", true, exclusive, false, false, nil)
	if err != nil {
		return fail(errors.Wrap(err, "amqp consume failed"))
	}

	return &EventSubscriber{
		onReadData: onReadData,
		amqpChan:   amqpChan,
		sigRead:    sigRead,
		sigDone:    make(chan struct{}),
	}, nil
}

func (s *EventSubscriber) Run() {
	for {
		select {
		case <-s.sigDone:
			s.amqpChan.Close()
			return
		case msg := <-s.sigRead:
			s.onReadData(msg.RoutingKey, msg.Body)
		}
	}
}

func (s *EventSubscriber) Stop() {
	close(s.sigDone)
}
