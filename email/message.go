package email

import (
	"encoding/base64"
	"fmt"
)

type Message struct {
	From    string `json:"from"`
	To      string `json:"to"`
	CC      string `json:"cc"`
	Subject string `json:"subject"`
	Body    string `json:"body"`
}

func (m *Message) toData() []byte {
	var h string
	writeHeader := func(k string, v string) {
		h += k + ": " + v + "\r\n"
	}

	writeHeader("From", m.From)
	writeHeader("To", m.To)

	if m.CC != "" {
		writeHeader("CC", m.CC)
	}

	if m.Subject != "" {
		b64 := base64.StdEncoding.EncodeToString([]byte(m.Subject))
		writeHeader("Subject", fmt.Sprintf(`=?%s?b?%s?=`, "utf-8", string(b64)))
	}

	writeHeader("Content-Type", "text/plain; charset=utf-8")

	data := append([]byte{}, []byte(h)...)
	data = append(data, []byte("\r\n")...)
	data = append(data, []byte(m.Body)...)

	return data
}
