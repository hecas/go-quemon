package email

import (
	"crypto/tls"
	"net"
	"net/mail"
	"net/smtp"

	"github.com/pkg/errors"
)

type Sender struct {
	smtpAddr string
	auth     smtp.Auth
}

func NewSender(smtpAddr string, auth smtp.Auth) *Sender {
	return &Sender{
		smtpAddr: smtpAddr,
		auth:     auth,
	}
}

func (s *Sender) Send(msg *Message) error {
	// dial
	conn, err := smtp.Dial(s.smtpAddr)
	if err != nil {
		return errors.Wrap(err, "dial failed")
	}
	defer conn.Close()

	// tls
	if ok, _ := conn.Extension("STARTTLS"); ok {
		host, _, _ := net.SplitHostPort(s.smtpAddr)
		conf := tls.Config{
			ServerName:         host,
			InsecureSkipVerify: true,
		}
		if err := conn.StartTLS(&conf); err != nil {
			return errors.Wrap(err, "start tls failed")
		}
	}

	// auth
	if s.auth != nil {
		if err := conn.Auth(s.auth); err != nil {
			return errors.Wrap(err, "auth failed")
		}
	}

	// from
	from, err := mail.ParseAddress(msg.From)
	if err != nil {
		return errors.Wrap(err, "parse from failed")
	}
	if err := conn.Mail(from.Address); err != nil {
		return errors.Wrap(err, "mail from failed")
	}

	// to
	to, err := mail.ParseAddressList(msg.To)
	if err != nil {
		return errors.Wrap(err, "parse to failed")
	}
	for i := range to {
		if err := conn.Rcpt(to[i].Address); err != nil {
			return errors.Wrap(err, "rcpt to failed")
		}
	}

	// data
	w, err := conn.Data()
	if err != nil {
		return errors.Wrap(err, "data failed")
	}
	defer w.Close()

	// write
	if _, err := w.Write(msg.toData()); err != nil {
		return errors.Wrap(err, "write failed")
	}

	// quit
	if err := conn.Quit(); err != nil {
		return errors.Wrap(err, "quit failed")
	}

	return nil
}
