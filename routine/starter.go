package routine

var Starter starter

type starter struct{}

func (starter) Run(backgrounds ...Background) (Stopper, error) {
	stops := []func(){}
	stopAll := func() {
		for i := range stops {
			stops[len(stops)-1-i]()
		}
	}

	for i := range backgrounds {
		run, stop, err := backgrounds[i]()
		if err != nil {
			stopAll()
			return nil, err
		}

		go run()

		stops = append(stops, stop)
	}

	return stopAll, nil
}

func (starter) Call(foregrounds ...Foreground) error {
	for i := range foregrounds {
		run, err := foregrounds[i]()
		if err != nil {
			return err
		}

		run()
	}

	return nil
}
