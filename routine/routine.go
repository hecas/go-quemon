package routine

type Runner func()

type Stopper func()

type Foreground func() (Runner, error)

type Background func() (Runner, Stopper, error)
