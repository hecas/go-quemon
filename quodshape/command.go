package quodshape

import (
	"fmt"
	"regexp"
	"strings"
)

func Command(modulePath, serviceName, commandName string) error {
	reFirstCap := regexp.MustCompile("(.)([A-Z][a-z]+)")
	reAllCap := regexp.MustCompile("([a-z0-9])([A-Z])")
	cmdSnakeName := reFirstCap.ReplaceAllString(commandName, "${1}_${2}")
	cmdSnakeName = reAllCap.ReplaceAllString(cmdSnakeName, "${1}_${2}")
	cmdSnakeName = strings.ToLower(cmdSnakeName)

	param := templateCommandParam{
		ModulePath:  modulePath,
		ServiceName: serviceName,
		CommandName: commandName,
	}

	return shapeTemplate(templateCommandAppCommand, &param,
		fmt.Sprintf("pkg/%s/app/command/%s.go", serviceName, cmdSnakeName))
}
