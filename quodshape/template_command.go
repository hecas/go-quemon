package quodshape

type templateCommandParam struct {
	ModulePath  string
	ServiceName string
	CommandName string
}

const templateCommandAppCommand = `package command

import (
	"{{ .ModulePath }}/pkg/{{ .ServiceName }}/app"
	"bitbucket.org/hecas/go-quemon/msgerr"
	"bitbucket.org/hecas/go-quemon/querr"
)

type {{ .CommandName }}Command struct {
}

type {{ .CommandName }}Message struct {
	msgerr.Descriptor
	Result {{ .CommandName }}Result ` + "`json:\"result\"`" + `
}

type {{ .CommandName }}Result struct {
}

type {{ .CommandName }}Handler struct {
}

func New{{ .CommandName }}Handler(ctx app.Context) *{{ .CommandName }}Handler {
	return &{{ .CommandName }}Handler{}
}

func (h *{{ .CommandName }}Handler) Handle(cmd *{{ .CommandName }}Command) (*{{ .CommandName }}Message, error) {
	return &{{ .CommandName }}Message{
		Descriptor: msgerr.MakeDescriptor(msgerr.NewWith(
			querr.ErrOK(), "성공하였습니다.",
		)),
		Result: {{ .CommandName }}Result{},
	}, nil
}
`
