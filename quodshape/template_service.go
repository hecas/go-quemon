package quodshape

type templateServiceParam struct {
	ModulePath  string
	ServiceName string
}

const templateServiceModelModel = `package model
`

const templateServiceAppApp = `package app

type Context interface {
	Begin() error
	Commit() error
	Close() error
}
`

const templateServiceAdapterInfraInfra = `package infra

import (
	"github.com/go-redis/redis/v8"
	"xorm.io/xorm"
)

type Context interface {
	DBSession() *xorm.Session
	Redis() *redis.Client
}
`

const templateServiceAdapterControllerController = `package controller

import (
	"{{ .ModulePath }}/pkg/{{ .ServiceName }}/adapter/infra"
	"{{ .ModulePath }}/pkg/{{ .ServiceName }}/app"
	"bitbucket.org/hecas/go-quemon/routine"
	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	"github.com/streadway/amqp"
	"xorm.io/xorm"
)

type Context struct {
	Echo        *echo.Echo
	Backgrounds *[]routine.Background
	DB          *xorm.Engine
	Redis       *redis.Client
	AMQP        *amqp.Connection
}

type appContext struct {
	parent *Context
	dbs    *xorm.Session
}

func newAppContext(ctx *Context) *appContext {
	return &appContext{
		parent: ctx,
	}
}

var _ app.Context = &appContext{}
var _ infra.Context = &appContext{}

func (c *appContext) Begin() error {
	err := c.DBSession().Begin()
	return err
}

func (c *appContext) Commit() error {
	err := c.DBSession().Commit()
	return err
}

func (c *appContext) Close() error {
	err := c.DBSession().Close()
	return err
}

func (c *appContext) DBSession() *xorm.Session {
	if c.dbs == nil {
		c.dbs = c.parent.DB.NewSession()
	}

	return c.dbs
}

func (c *appContext) Redis() *redis.Client {
	return c.parent.Redis
}
`

const templateServiceAdapterControllerConfig = `package controller

func Setup(ctx *Context) error {
	setupHTTPHandler(ctx, func() []*httpHandler {
		return []*httpHandler{}
	}()...)

	wsCtxMgr := newWSContextManager()

	setupWSHandler(ctx, wsCtxMgr, func() []*wsHandler {
		return []*wsHandler{}
	}()...)

	setupWorkerHandler(ctx, wsCtxMgr, func() []*workerHandler {
		return []*workerHandler{}
	}()...)

	return nil
}
`

const templateServiceAdapterControllerHttp = `package controller

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

type httpHandler struct {
	methods []string
	path    string
	handler echo.HandlerFunc
}

func setupHTTPHandler(ctx *Context, handlers ...*httpHandler) {
	setup := func(method string, path string, handler echo.HandlerFunc) {
		ctx.Echo.Add(method, path, handler, func(next echo.HandlerFunc) echo.HandlerFunc {
			return func(echoCtx echo.Context) error {
				echoCtx.Set("parent", ctx)

				callNext := func() error {
					return next(echoCtx)
				}

				return callNext()
			}
		})
	}

	for _, handler := range handlers {
		for _, method := range handler.methods {
			log.Infof("HTTP %s %s", method, handler.path)
			setup(method, handler.path, handler.handler)
		}
	}
}
`

const templateServiceAdapterControllerWorker = `package controller

import (
	"bitbucket.org/hecas/go-quemon/routine"
	"github.com/labstack/gommon/log"
)

type workerHandler struct {
	name       string
	makeRunner func(*workerContext) (routine.Runner, error)
}

func setupWorkerHandler(ctx *Context, wsCtxMgr *wsContextManager, handlers ...*workerHandler) {
	setup := func(handler *workerHandler) {
		*ctx.Backgrounds = append(*ctx.Backgrounds, func() (routine.Runner, routine.Stopper, error) {
			sigDone := make(chan struct{})

			workerCtx := newWorkerContext(ctx, sigDone, wsCtxMgr)

			run, err := handler.makeRunner(workerCtx)
			if err != nil {
				return nil, nil, err
			}

			stop := func() {
				close(sigDone)
			}

			return run, stop, nil
		})
	}

	for i := range handlers {
		log.Infof("Worker %s", handlers[i].name)
		setup(handlers[i])
	}
}

type workerContext struct {
	parent   *Context
	sigDone  chan struct{}
	wsCtxMgr *wsContextManager
}

func newWorkerContext(parentCtx *Context, sigDone chan struct{}, wsCtxMgr *wsContextManager) *workerContext {
	return &workerContext{
		parent:   parentCtx,
		sigDone:  sigDone,
		wsCtxMgr: wsCtxMgr,
	}
}
`

const templateServiceAdapterControllerWs = `package controller

import (
	"io"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

type wsHandler struct {
	path        string
	onConnected func(ctx *wsContext) error
	onClosed    func(ctx *wsContext)
	onReadData  func(ctx *wsContext, reader io.Reader, binary bool) error
}

func setupWSHandler(ctx *Context, wsCtxMgr *wsContextManager, handlers ...*wsHandler) {
	upgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	writeChBufferSize := 32
	writeWait := 10 * time.Second
	pongWait := 30 * time.Second
	pingPeriod := pongWait * 4 / 5

	setup := func(handler *wsHandler) {
		ctx.Echo.Add(echo.GET, handler.path, func(echoCtx echo.Context) error {
			conn, err := upgrader.Upgrade(echoCtx.Response(), echoCtx.Request(), nil)
			if err != nil {
				return err
			}
			defer conn.Close()

			echoCtx.Set("parent", ctx)

			sigWrite := make(chan []byte, writeChBufferSize)
			sigVisit := make(chan func())

			sessCtx := newWSContext(echoCtx, sigWrite, sigVisit)

			wsCtxMgr.onConnected(sessCtx)
			if handler.onConnected != nil {
				if err := handler.onConnected(sessCtx); err != nil {
					return err
				}
			}
			defer func() {
				wsCtxMgr.onDisconnected(sessCtx)
				if handler.onClosed != nil {
					handler.onClosed(sessCtx)
				}
			}()

			sigDone := make(chan struct{})

			// writer
			go func() {
				ticker := time.NewTicker(pingPeriod)
				defer func() {
					ticker.Stop()
				}()

				for {
					select {
					case <-sigDone:
						conn.WriteMessage(websocket.CloseMessage, []byte{})

						return
					case bytes := <-sigWrite:
						conn.SetWriteDeadline(time.Now().Add(writeWait))

						w, err := conn.NextWriter(websocket.TextMessage)
						if err != nil {
							return
						}

						w.Write(bytes)

						// flush
						if err := w.Close(); err != nil {
							return
						}
					case <-ticker.C:
						conn.SetWriteDeadline(time.Now().Add(writeWait))

						if err := conn.WriteMessage(websocket.PingMessage, nil); err != nil {
							return
						}
					}
				}
			}()

			// reader
			go func() {
				defer func() {
					close(sigDone)
				}()

				conn.SetReadDeadline(time.Now().Add(pongWait))
				conn.SetPongHandler(func(string) error {
					conn.SetReadDeadline(time.Now().Add(pongWait))

					return nil
				})

				for {
					typ, reader, err := conn.NextReader()
					if err != nil {
						return
					}

					if typ != websocket.TextMessage && typ != websocket.BinaryMessage {
						continue
					}

					if handler.onReadData != nil {
						sigErr := make(chan error)
						sessCtx.visit(func() {
							sigErr <- handler.onReadData(sessCtx, reader, typ == websocket.BinaryMessage)
						})

						if err := <-sigErr; err != nil {
							log.Error(err)
							return
						}
					}
				}
			}()

			// not to return echoCtx
			for {
				select {
				case <-sigDone:
					return nil
				case job := <-sigVisit:
					job()
				}
			}
		})
	}

	for i := range handlers {
		log.Infof("WS %s", handlers[i].path)
		setup(handlers[i])
	}
}

type wsContext struct {
	echo.Context
	sigWritten chan []byte
	sigVisited chan func()
}

func newWSContext(
	ctx echo.Context,
	sigWritten chan []byte,
	sigVisited chan func(),
) *wsContext {
	return &wsContext{
		Context:    ctx,
		sigWritten: sigWritten,
		sigVisited: sigVisited,
	}
}

func (c *wsContext) parent() *Context {
	return c.Get("parent").(*Context)
}

func (c *wsContext) write(b []byte) {
	c.sigWritten <- b
}

func (c *wsContext) visit(job func()) {
	c.sigVisited <- job
}

type wsContextManager struct {
	mu       sync.RWMutex
	sessions map[*wsContext]struct{}
}

func newWSContextManager() *wsContextManager {
	return &wsContextManager{
		sessions: map[*wsContext]struct{}{},
	}
}

func (m *wsContextManager) onConnected(ctx *wsContext) {
	m.mu.Lock()
	defer m.mu.Unlock()

	m.sessions[ctx] = struct{}{}
}

func (m *wsContextManager) onDisconnected(ctx *wsContext) {
	m.mu.Lock()
	defer m.mu.Unlock()

	delete(m.sessions, ctx)
}

func (m *wsContextManager) visit(job func(map[*wsContext]struct{})) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	job(m.sessions)
}
`
