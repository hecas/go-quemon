package quodshape

import "fmt"

func Service(modulePath, serviceName string) error {
	param := templateServiceParam{
		ModulePath:  modulePath,
		ServiceName: serviceName,
	}

	err := serviceModel(&param)
	if err != nil {
		return err
	}

	err = serviceApp(&param)
	if err != nil {
		return err
	}

	err = serviceAdapter(&param)
	if err != nil {
		return err
	}

	return err
}

func serviceModel(param *templateServiceParam) error {
	return shapeTemplate(templateServiceModelModel, param,
		fmt.Sprintf("pkg/%s/model/model.go", param.ServiceName))
}

func serviceApp(param *templateServiceParam) error {
	return shapeTemplate(templateServiceAppApp, param,
		fmt.Sprintf("pkg/%s/app/app.go", param.ServiceName))
}

func serviceAdapter(param *templateServiceParam) error {
	err := serviceAdapterInfra(param)
	if err != nil {
		return err
	}

	err = serviceAdapterController(param)
	if err != nil {
		return err
	}

	return nil
}

func serviceAdapterInfra(param *templateServiceParam) error {
	err := shapeTemplate(templateServiceAdapterInfraInfra, param,
		fmt.Sprintf("pkg/%s/adapter/infra/infra.go", param.ServiceName))
	if err != nil {
		return err
	}

	return nil
}

func serviceAdapterController(param *templateServiceParam) error {
	err := shapeTemplate(templateServiceAdapterControllerHttp, param,
		fmt.Sprintf("pkg/%s/adapter/controller/http.go", param.ServiceName))
	if err != nil {
		return err
	}

	err = shapeTemplate(templateServiceAdapterControllerWorker, param,
		fmt.Sprintf("pkg/%s/adapter/controller/worker.go", param.ServiceName))
	if err != nil {
		return err
	}

	err = shapeTemplate(templateServiceAdapterControllerWs, param,
		fmt.Sprintf("pkg/%s/adapter/controller/ws.go", param.ServiceName))
	if err != nil {
		return err
	}

	err = shapeTemplate(templateServiceAdapterControllerController, param,
		fmt.Sprintf("pkg/%s/adapter/controller/controller.go", param.ServiceName))
	if err != nil {
		return err
	}

	err = shapeTemplate(templateServiceAdapterControllerConfig, param,
		fmt.Sprintf("pkg/%s/adapter/controller/config.go", param.ServiceName))
	if err != nil {
		return err
	}

	return nil
}
