package quodshape

import (
	"os"
	"path"
	"text/template"
)

func shapeTemplate(tplStr string, v interface{}, filepath string) error {
	err := os.MkdirAll(path.Dir(filepath), 0755)
	if err != nil {
		return err
	}

	f, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer f.Close()

	tpl, err := template.New("").Parse(tplStr)
	if err != nil {
		return err
	}

	err = tpl.Execute(f, v)
	if err != nil {
		return err
	}

	return nil
}
