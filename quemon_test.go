package quemon_test

import (
	"testing"

	"bitbucket.org/hecas/go-quemon/toolqit"
	"go.uber.org/goleak"
)

func TestToolqit(t *testing.T) {
	toolqit.Toolqit("bitbucket.org/hecas/go-quemon", "", "shape", "service", "test")
}

func TestChannel(t *testing.T) {
	defer goleak.VerifyNone(t)
	doneCh := make(chan struct{})

	select {
	case doneCh <- struct{}{}:
	default:
		t.Logf("default")
	}
}
